<?php

    Route::group(
        [
            'prefix' => 'auth',
        ],
        static function () {
            Route::post('login', 'Api\AuthController@login');
            Route::post('register', 'Api\AuthController@signup');

            Route::group(
                [
                    'middleware' => 'auth:api',
                ],
                static function () {
                    Route::get('logout', 'Api\AuthController@logout');
                    Route::get('me', 'Api\AuthController@user');
                }
            );
        }

    );

    Route::resource('countries', 'Api\CountryController');
    Route::resource('cities', 'Api\CityController');

    Route::group(
        [
            'middleware' => 'auth:api',
        ],
        static function () {

            Route::resource('notifications', 'Api\NotificationController');
            Route::resource('survey', 'Api\SurveyController');
            Route::resource('categories', 'Api\CategoryController');
            Route::resource('home-elements', 'Api\HomeElementController');
            Route::resource('consultation-forms', 'Api\ConsultationFormController');
            Route::resource('consultation-titles', 'Api\ConsultationTitleController');
            Route::resource('doctors', 'Api\DoctorController');
            Route::resource('events', 'Api\EventController');
            Route::resource('faqs', 'Api\FaqController');
            Route::resource('hospitals', 'Api\HospitalController');
            Route::resource('professions', 'Api\ProfessionController');
            Route::resource('publishes', 'Api\PublishController');
            Route::resource('recommendations', 'Api\RecommendationController');
            Route::resource('reminders', 'Api\ReminderController');
            Route::resource('social-life-categories', 'Api\SocialLifeCategoryController');
            Route::resource('social-life-places', 'Api\SocialLifePlaceController');
            Route::resource('static-contents', 'Api\StaticContentController');
            Route::resource('users', 'Api\UserController');
            Route::resource('user-favorites', 'Api\UserFavoriteController');
            Route::resource('videos', 'Api\VideoController');
            Route::resource('video-categories', 'Api\VideoCategoryController');
            Route::resource('search', 'Api\SearchController');
            Route::get('popular-search', 'Api\HelperController@popularSearch');
        }
    );

    Route::post('forget', 'Api\ForgetController@reset');

    Route::get('/', static function () {
        return [
            'v'      => 1,
            'status' => 'OK',
        ];
    });
