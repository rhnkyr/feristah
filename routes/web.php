<?php

    // Authentication Routes...
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login-form');
    Route::post('login', 'Auth\LoginController@login')->name('login');
    Route::post('log-out', 'Auth\LoginController@logout')->name('logout');

    //Admin routes
    Route::namespace('Admin')
        ->name('admin.')
        ->prefix('admin')
        ->middleware(['auth', 'revalidate'])
        ->group(static function () {

            Route::resource('admins', 'AdminController');
            Route::get('inside', 'AdminController@inside')->name('inside.index');
            Route::resource('home-elements', 'AppHomeController');
            Route::post('home-elements/sort-f', 'AppHomeController@sortFirstRow')->name('home-screens.sort.f');
            Route::post('home-elements/sort-s', 'AppHomeController@sortSecondRow')->name('home-screens.sort.s');
            Route::resource('categories', 'CategoryController');
            Route::resource('consultation-forms', 'ConsultationFormController');
            Route::resource('consultation-titles', 'ConsultationTitleController');
            Route::resource('doctors', 'DoctorController');
            Route::resource('events', 'EventController');
            Route::resource('faqs', 'FaqController');
            Route::post('faqs/sort', 'FaqController@sort')->name('faqs.sort');
            Route::resource('hospitals', 'HospitalController');
            Route::resource('professions', 'ProfessionController');
            Route::resource('publishes', 'PublishController');
            Route::post('publishes/sort', 'PublishController@sort')->name('publishes.sort');
            Route::resource('recommendations', 'RecommendationController');
            Route::resource('social-life-categories', 'SocialLifeCategoryController');
            Route::post('social-life-categories/sort',
                'SocialLifeCategoryController@sort')->name('social-life-categories.sort');
            Route::resource('social-life-places', 'SocialLifePlaceController');
            Route::resource('static-contents', 'StaticContentController');
            Route::resource('users', 'UserController');
            Route::resource('video-categories', 'VideoCategoryController');
            Route::post('video-categories/sort', 'VideoCategoryController@sort')->name('video-categories.sort');
            Route::resource('videos', 'VideoController');
            Route::get('notification', 'NotificationController@index')->name('notification');
            Route::post('send-notification', 'NotificationController@send')->name('send-notification');

        });

    //Index route
    Route::get('/', static function () {
        return view('welcome');
    });
