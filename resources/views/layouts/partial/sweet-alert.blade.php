<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script>
    $(function () {

        $('.btn-danger').on('click', function (e) {
            e.preventDefault();

            swal({
                buttons: ["Vazgeç", "Evet"],
                title: "Emin misiniz?",
                //text: "Once deleted, you will not be able to recover this imaginary file!",
                icon: "warning",
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {

                        let url = $(this).data('url');
                        $.ajax({
                            url: url,
                            type: 'DELETE',
                            data: {
                                "_token": "{{ csrf_token() }}"
                            },
                            success: function (result) {
                                if (result.status) {
                                    location.reload();
                                }
                            }
                        });
                    }
                });
        })
    })
</script>
