<div class="page-sidebar navbar-collapse collapse">
    <!-- BEGIN SIDEBAR MENU -->
    <ul class="page-sidebar-menu  page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true"
        data-slide-speed="200">
        <li class="nav-item {{ active('admin.inside.index') }}">
            <a href="{{route('admin.inside.index')}}" class="nav-link nav-toggle">
                <i class="fa fa-home"></i>
                <span class="title">Güncel</span>
            </a>
        </li>
        <li class="nav-item {{ active('admin.home-elements.*') }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-columns"></i>
                <span class="title">Uygulama Ana Sayfa</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.home-elements.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.home-elements.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active('admin.admins.*') }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user-plus"></i>
                <span class="title">Yöneticiler</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.admins.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.admins.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active('admin.users.*') }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-users"></i>
                <span class="title">Uygulama Kullanıcıları</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.users.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
            <!--<li class="nav-item  ">
                    <a href="{{route('admin.users.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>-->
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.professions.*', 'admin.doctors.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-medkit"></i>
                <span class="title">Doktorlar</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.professions.index')}}" class="nav-link ">
                        <span class="title">Uzmanlık Alanları Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.professions.create')}}" class="nav-link ">
                        <span class="title">Yeni Uzmanlık Alanı Ekle</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.doctors.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.doctors.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.hospitals.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-hospital-o"></i>
                <span class="title">Hastaneler</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.hospitals.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.hospitals.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item {{ active(['admin.consultation-titles.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-question"></i>
                <span class="title">Danışmanlık Başlıkları</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.consultation-titles.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.consultation-titles.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item {{ active(['admin.faqs.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-list-ul"></i>
                <span class="title">SSS</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.faqs.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.faqs.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.publishes.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-book"></i>
                <span class="title">Yayınlar</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.publishes.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.publishes.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.events.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-clock-o"></i>
                <span class="title">Etkinlikler</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.events.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.events.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.videos.*', 'admin.video-categories.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-vimeo"></i>
                <span class="title">Video</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.video-categories.index')}}" class="nav-link ">
                        <span class="title">Kategori Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.video-categories.create')}}" class="nav-link ">
                        <span class="title">Yeni Kategori Ekle</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.videos.index')}}" class="nav-link ">
                        <span class="title">Video Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.videos.create')}}" class="nav-link ">
                        <span class="title">Yeni Video Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.social-life-places.*', 'admin.social-life-categories.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-map-pin"></i>
                <span class="title">Sosyal Yaşam</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.social-life-categories.index')}}" class="nav-link ">
                        <span class="title">Kategori Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.social-life-categories.create')}}" class="nav-link ">
                        <span class="title">Yeni Kategori Ekle</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.social-life-places.index')}}" class="nav-link ">
                        <span class="title">Mekan Listele</span>
                    </a>
                </li>
                <li class="nav-item  ">
                    <a href="{{route('admin.social-life-places.create')}}" class="nav-link ">
                        <span class="title">Yeni Mekan Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active(['admin.static-contents.*']) }}">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-file-text"></i>
                <span class="title">İçerik</span>
                <span class="arrow"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  ">
                    <a href="{{route('admin.static-contents.index')}}" class="nav-link ">
                        <span class="title">Listele</span>
                    </a>
                </li>
            <li class="nav-item  ">
                    <a href="{{route('admin.static-contents.create')}}" class="nav-link ">
                        <span class="title">Yeni Ekle</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-item {{ active('admin.notification') }}">
            <a href="{{route('admin.notification')}}" class="nav-link nav-toggle">
                <i class="fa fa-send-o"></i>
                <span class="title">Bildirim Gönder</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{ route('logout') }}" class="nav-link nav-toggle"
               onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                <i class="fa fa-lock"></i>
                <span class="title">Oturumu Kapat</span>
            </a>
            <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>
    </ul>
    <!-- END SIDEBAR MENU -->
</div>
