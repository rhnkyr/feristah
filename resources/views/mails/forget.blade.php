<html>

<body
    style="background-color:#e2e1e0;font-family: Open Sans, sans-serif;font-size:100%;font-weight:400;line-height:1.4;color:#000;">
<table
    style="max-width:670px;margin:50px auto 10px;background-color:#fff;padding:50px;-webkit-border-radius:3px;-moz-border-radius:3px;border-radius:3px;-webkit-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);-moz-box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24);box-shadow:0 1px 3px rgba(0,0,0,.12),0 1px 2px rgba(0,0,0,.24); border-top: solid 10px blue;">

    <tbody>
    <tr>
        <td style="height:35px;">
            <h1 style="text-align: center">Geçici Şifreniz</h1>
        </td>
    </tr>

    <tr>
        <td style="width:50%;padding:20px;vertical-align:top">
            <h3 style="margin:0 0 10px 0;padding:0;font-size:35px; text-align: center">{{$password}}</h3>
        </td>
    </tr>

    </tbody>

</table>
</body>

</html>
