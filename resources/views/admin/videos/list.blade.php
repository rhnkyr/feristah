@extends('layouts.app')

@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('content')


    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Videolar
                        @if($rows instanceof \Illuminate\Pagination\LengthAwarePaginator)
                            ({{$rows->total()}})
                        @endif
                        <small>Liste</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box">
                <div class="portlet-body">
                    <form role="form" action="{{route('admin.videos.index')}}">
                        <div class="form-group">
                            <div class="input-group">
                                <input type="text" name="video_name" class="form-control"
                                       placeholder="Video başlığına göre...">
                                <span class="input-group-btn">
                                   <button class="btn green" type="submit">ARA</button>
                                </span>
                            </div>
                            <!-- /input-group -->
                        </div>
                    </form>
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th> Kategori</th>
                                <th> Başlık</th>
                                <th> Durum</th>
                                <th> İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $row)
                                <tr>
                                    <td> {{$row->category->title}}</td>
                                    <td> {{$row->title}}</td>
                                    <td>
                                        @if($row->status === 1)
                                            <span class="label label-sm label-success"> Aktif </span>
                                        @else
                                            <span class="label label-sm label-warning"> Pasif </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('admin.videos.edit', ['id' => $row->id])}}"
                                           class="btn btn-xs btn-info">Güncelle</a>
                                        <a href="#"
                                           data-url="{{route('admin.videos.destroy', ['id' => $row->id])}}"
                                           class="btn btn-xs btn-danger">Sil</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                    @if($rows instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <div class="row">
                            <div class="col-md-12">
                                {{ $rows->links() }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection

@section('page_extra_js')
    @if(session('success'))
        @include('layouts.partial.toastr')
    @endif
    @include('layouts.partial.sweet-alert')
@endsection
