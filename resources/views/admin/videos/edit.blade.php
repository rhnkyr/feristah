@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Video
                        <small>Güncelle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.videos.update',['id'=>$data->id])}}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="form-body">
                            <div class="form-group{{ $errors->has('video_category_id') ? ' has-error' : '' }}"
                                 style="width: 35%">
                                <label class="control-label">Kategori</label>
                                <select name="video_category_id" class="form-control input-lg select2">
                                    @foreach($categories as $category)
                                        <option
                                            @if(old('video_category_id', $data->video_category_id) === $category->id) selected
                                            @endif value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('video_category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video_category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label">Başlık</label>
                                <input type="text" class="form-control input-lg" name="title"
                                       value="{{old('title', $data->title)}}"/>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('source') ? ' has-error' : '' }}">
                                <label class="control-label">Kaynak URL (Örn : https://vimeo.com/XXXXXXX)</label>
                                <input type="text" class="form-control input-lg" name="source"
                                       value="{{old('source', $data->source)}}"/>
                                @if ($errors->has('source'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('source') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="control-label">İçerik</label>
                                <textarea id="summernote" name="description"
                                          class="form-control">{{old('description', $data->description)}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label">Durum</label>
                                <select class="form-control input-lg" style="width: 25%" name="status">
                                    <option value="1" @if(old('status', $data->status) === 1) selected @endif >Aktif
                                    </option>
                                    <option value="0" @if(old('status', $data->status) === 0) selected @endif >Pasif
                                    </option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/lang/summernote-tr-TR.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#summernote').summernote(
                {
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                    ],
                    tabsize: 2,
                    height: 300,
                    lang: 'tr-TR'
                }
            );
        });
    </script>
    <!-- END PAGE LEVEL SCRIPTS -->

@endsection
