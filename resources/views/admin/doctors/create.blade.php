@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Doktor
                        <small>Ekle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.doctors.store')}}" method="post">
                        @csrf
                        <div class="form-body">
                            <div class="form-group{{ $errors->has('profession_id') ? ' has-error' : '' }}"
                                 style="width: 35%">
                                <label class="control-label">Uzmanlık Alanı</label>
                                <select name="profession_id" class="form-control input-lg select2">
                                    @foreach($professions as $profession)
                                        <option @if(old('profession_id') === $profession->id) selected @endif value="{{$profession->id}}">{{$profession->title}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('profession_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('profession_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group" style="width: 35%">
                                <label class="control-label">Hastane</label>
                                <select name="hospital_id" class="form-control input-lg select2">
                                    <option value="0">Bağımsız</option>
                                    @foreach($hospitals as $hospital)
                                        <option @if(old('hospital_id') === $hospital->id) selected @endif value="{{$hospital->id}}">{{$hospital->title}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group{{ $errors->has('city_id') ? ' has-error' : '' }}" style="width: 35%">
                                <label class="control-label">Şehir</label>
                                <select id="single" name="city_id" class="form-control input-lg select2">
                                    @foreach($cities as $city)
                                        <option @if(old('city_id') === $city->id) selected @endif value="{{$city->id}}">{{$city->name}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('city_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('city_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label">Adı</label>
                                <input type="text" class="form-control input-lg" name="name" value="{{old('name')}}"/>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('sub_title') ? ' has-error' : '' }}">
                                <label class="control-label">Alt Başlık</label>
                                <input type="text" class="form-control input-lg" name="sub_title" value="{{old('sub_title')}}"/>
                                @if ($errors->has('sub_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label">Durum</label>
                                <select class="form-control input-lg" style="width: 25%" name="status">
                                    <option value="1" @if(old('status') === 1) selected @endif >Aktif</option>
                                    <option value="0" @if(old('status') === 0) selected @endif >Pasif</option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
@endsection
