@extends('layouts.app')

@section('page_extra_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sıkça Sorulan Soru
                        <small>Güncelle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.faqs.update',['id'=>$data->id])}}" method="post">
                        @csrf
                        @method('PATCH')
                        <div class="form-body">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label">Başlık</label>
                                <input type="text" class="form-control input-lg" name="title" value="{{old('title',$data->title)}}"/>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('content') ? ' has-error' : '' }}">
                                <label class="control-label">İçerik</label>
                                <textarea id="summernote" name="content"
                                          class="form-control">{{old('content',$data->content)}}</textarea>
                                @if ($errors->has('content'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label">Durum</label>
                                <select class="form-control input-lg" style="width: 25%" name="status">
                                    <<option value="1" @if(old('status', $data->status) === 1) selected @endif >Aktif
                                    </option>
                                    <option value="0" @if(old('status', $data->status) === 0) selected @endif >Pasif
                                    </option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/lang/summernote-tr-TR.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#summernote').summernote(
                {
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                    ],
                    tabsize: 2,
                    height: 300,
                    lang: 'tr-TR'
                }
            );
        });
    </script>
@endsection
