@extends('layouts.app')

@section('page_extra_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sosyal Yaşam Kategori
                        <small>Güncelle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.social-life-categories.update',['id'=>$data->id])}}"
                          method="post"
                          enctype="multipart/form-data">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="order" value="1">
                        <div class="form-body">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label">Başlık</label>
                                <input type="text" class="form-control input-lg" name="title"
                                       value="{{old('title',$data->title)}}"/>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('image_path') ? ' has-error' : '' }}">
                                <label class="control-label">Görsel</label>
                                <input type="hidden" name="old_image_path" value="{{$data->image_path}}">
                                <div class="input-group input-file" name="image_path" style="width: 50%">
                                    <input type="text" class="form-control" placeholder='Dosya seçiniz...'/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-choose" type="button">Seç</button>
                                    </span>
                                    @if ($errors->has('image_path'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('image_path') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row margin-top-15" style="margin-left: 0">
                                <img src="/storage/public/{{$data->image_path}}" alt="" style="width: 300px">
                            </div>
                            <div class="form-group{{ $errors->has('icon_path') ? ' has-error' : '' }}">
                                <label class="control-label">İkon</label>
                                <input type="hidden" name="old_icon_path" value="{{$data->icon_path}}">
                                <div class="input-group input-file" name="icon_path" style="width: 50%">
                                    <input type="text" class="form-control" placeholder='Dosya seçiniz...'/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-choose" type="button">Seç</button>
                                    </span>
                                    @if ($errors->has('icon_path'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('icon_path') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="row margin-top-15" style="margin-left: 0">
                                <img src="/storage/public/{{$data->icon_path}}" alt="" style="width: 300px">
                            </div>
                            <div class="form-group {{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label">Durum</label>
                                <select class="form-control input-lg" style="width: 25%" name="status">
                                    <option value="1" @if(old('status', $data->status) === 1) selected @endif >Aktif
                                    </option>
                                    <option value="0" @if(old('status', $data->status) === 0) selected @endif >Pasif
                                    </option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/lang/summernote-tr-TR.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#summernote').summernote(
                {
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                    ],
                    tabsize: 2,
                    height: 300,
                    lang: 'tr-TR'
                }
            );

            bs_input_file();
        });

        function bs_input_file() {
            $(".input-file").before(
                function () {
                    if (!$(this).prev().hasClass('input-ghost')) {
                        let element = $("<input type='file' class='input-ghost' accept='image/jpeg,image/png,image/jpg,image/svg' style='visibility:hidden; height:0'>");
                        element.attr("name", $(this).attr("name"));
                        element.change(function () {
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function () {
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function () {
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor", "pointer");
                        $(this).find('input').mousedown(function () {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
    </script>
@endsection
