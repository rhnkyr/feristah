@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Sosyal Yaşam Mekan
                        <small>Ekle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.social-life-places.store')}}" method="post">
                        @csrf
                        <div class="form-body">
                            <div class="form-group{{ $errors->has('social_life_category_id') ? ' has-error' : '' }}"
                                 style="width: 35%">
                                <label class="control-label">Kategori</label>
                                <select name="social_life_category_id" class="form-control input-lg select2">
                                    @foreach($categories as $category)
                                        <option @if(old('social_life_category_id') === $category->id) selected @endif value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>
                                @if ($errors->has('social_life_category_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('social_life_category_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label class="control-label">Adı</label>
                                <input type="text" class="form-control input-lg" name="name" value="{{old('name')}}"/>
                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('sub_title') ? ' has-error' : '' }}">
                                <label class="control-label">Alt Başlık</label>
                                <input type="text" class="form-control input-lg" name="sub_title" value="{{old('sub_title')}}"/>
                                @if ($errors->has('sub_title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sub_title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('address') ? ' has-error' : '' }}">
                                <label class="control-label">Adres</label>
                                <input type="text" class="form-control input-lg" name="address" value="{{old('address')}}"/>
                                @if ($errors->has('address'))
                                    <span class="help-block">a
                                        <strong>{{ $errors->first('address') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('location') ? ' has-error' : '' }}">
                                <label class="control-label">Lokasyon (Koordinatları arada virgül ile yazınız. Örn :
                                    41.1212,29.1212)</label>
                                <input type="text" class="form-control input-lg" style="width: 50%" value="{{old('location')}}"
                                       name="location"/>
                                @if ($errors->has('location'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('location') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
                                <label class="control-label">Durum</label>
                                <select name="status" class="form-control input-lg" style="width: 25%">
                                    <option value="1" @if(old('status') === 1) selected @endif >Aktif</option>
                                    <option value="0" @if(old('status') === 0) selected @endif >Pasif</option>
                                </select>
                                @if ($errors->has('status'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
@endsection
