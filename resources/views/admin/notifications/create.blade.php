@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Uygulamaya Bildirim
                        <small>Gönder</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.send-notification')}}" method="post">
                        @csrf
                        <div class="form-body">

                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label">Başlık</label>
                                <input type="text" class="form-control input-lg" name="title" value="{{old('title')}}"/>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="control-label">Metin</label>
                                <textarea name="message" style="height: 300px"
                                          class="form-control">{{old('message')}}</textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('content') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Gönder</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
@if(session('success'))
    @include('layouts.partial.toastr')
@endif
@endsection
