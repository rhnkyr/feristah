@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')


    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Statik İçerik
                        <small>Liste</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th> Başlık</th>
                                <th> İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $row)
                                <tr>
                                    <td class="align-middle"> {{$row->title}}</td>
                                    <td>
                                        <a href="{{route('admin.static-contents.edit', ['id' => $row->id])}}"
                                           class="btn btn-xs btn-info">Güncelle</a>
                                        <a href="#"
                                           data-url="{{route('admin.static-contents.destroy', ['id' => $row->id])}}"
                                           class="btn btn-xs btn-danger">Sil</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection

@section('page_extra_js')
    @if(session('success'))
        @include('layouts.partial.toastr')
    @endif
    @include('layouts.partial.sweet-alert')
@endsection
