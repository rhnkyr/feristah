@extends('layouts.app')

@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection

@section('content')

    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Ana Sayfa Elementler</h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="note note-info">
                <h3 class="block">Bilgi!</h3>
                <h4> Yeni bir içerik eklediğinizde, sürükle bırak yaparak içeriğin sırasını belrleyiniz!</h4>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">İlk Satır</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="mt-element-list">
                        <div class="mt-list-container list-simple">
                            <ul class="fr">
                                @foreach($first_row_elements as $first_row_element)
                                    <li class="mt-list-item" id="items-{{$first_row_element->id}}">
                                        <div class="list-datetime">
                                            <a href="#"
                                               data-url="{{route('admin.home-elements.destroy', ['id' => $first_row_element->id])}}"
                                               class="btn btn-xs btn-danger">Sil</a>
                                        </div>
                                        <img style="width: 100px" alt="" src="/storage/public/{{$first_row_element->image_path}}">
                                        <div class="list-item-content">
                                            <h3 style="float: left;margin-top: -41px;margin-left: 50px;">
                                                <a href="javascript:;">
                                                    {{$first_row_element->title}}
                                                </a>
                                            </h3>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="portlet light portlet-fit bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class=" icon-layers font-green"></i>
                        <span class="caption-subject font-green bold uppercase">İkinci Satır</span>
                    </div>
                </div>
                <div class="portlet-body">
                    <div class="mt-element-list">
                        <div class="mt-list-container list-simple">
                            <ul class="sr">
                                @foreach($second_row_elements as $second_row_element)
                                    <li class="mt-list-item" id="items-{{$second_row_element->id}}">
                                        <div class="list-datetime">
                                            <a href="#"
                                               data-url="{{route('admin.home-elements.destroy', ['id' => $second_row_element->id])}}"
                                               class="btn btn-xs btn-danger">Sil</a>
                                        </div>
                                        <img style="width: 100px" alt="" src="/storage/public/{{$second_row_element->image_path}}">
                                        <div class="list-item-content">
                                            <h3 style="float: left;margin-top: -41px;margin-left: 50px;">
                                                <a href="javascript:;">
                                                    {{$second_row_element->title}}
                                                </a>
                                            </h3>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- END PAGE BASE CONTENT -->
@endsection

@section('page_extra_js')
    @if(session('success'))
        @include('layouts.partial.toastr')
    @endif
    @include('layouts.partial.sweet-alert')
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.fr').sortable({
            update: function (event, ui) {
                let data = $(this).sortable('serialize');

                // POST to server using $.post or $.ajax
                $.ajax({
                    url: '{{route('admin.home-screens.sort.f')}}',
                    type: 'POST',
                    data: data,
                    success: function (result) {
                        if (result.status) {
                            location.reload();
                        }
                    }
                });
            }
        });
        $('.sr').sortable({
            update: function (event, ui) {
                let data = $(this).sortable('serialize');

                // POST to server using $.post or $.ajax
                $.ajax({
                    url: '{{route('admin.home-screens.sort.s')}}',
                    type: 'POST',
                    data: data,
                    success: function (result) {
                        if (result.status) {
                            location.reload();
                        }
                    }
                });
            }
        });
    </script>

@endsection
