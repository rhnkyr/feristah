@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/select2/css/select2.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/select2/css/select2-bootstrap.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Element
                        <small>Ekle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.home-elements.store')}}" method="post"
                          enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">

                            <div class="form-group{{ $errors->has('row') ? ' has-error' : '' }}">
                                <label class="control-label">Yeri</label>
                                <select name="row" class="form-control input-lg" style="width: 25%">
                                    <option value="1" @if(old('row') === 1) selected @endif >İlk Satır</option>
                                    <option value="2" @if(old('row') === 2) selected @endif >İkinci Satır</option>
                                </select>
                                @if ($errors->has('row'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('row') }}</strong>
                                    </span>
                                @endif
                            </div>


                            <div class="form-group{{ $errors->has('data') ? ' has-error' : '' }}">
                                <label class="control-label">İçerik</label>
                                <select name="data" class="form-control input-lg" style="width: 25%">

                                    <optgroup label="Daktorlar">
                                        <option value="doctorsPage|Daktorlar - Ana Sayfa">Daktorlar - Ana Sayfa</option>
                                    </optgroup>

                                    <optgroup label="Hastaneler">
                                        <option value="hospitalPage|Hastaneler - Ana Sayfa">Hastaneler - Ana Sayfa</option>
                                    </optgroup>

                                    <optgroup label="Danışmalık (Kategorileri)">
                                        <option value="consultancyPage|Danışmalık - Ana Sayfa">Danışmalık - Ana Sayfa</option>
                                        @foreach($consultations as $consultation)
                                            <option
                                                value="{{$consultation->id.'|App\Models\ConsultationTitle|consultancy|Danışmalık - '.$consultation->title}}">Danışmalık - {{$consultation->title}}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Etkinlikler">
                                        <option value="eventPage|Etkinlikler - Ana Sayfa">Etkinlikler - Ana Sayfa</option>
                                        @foreach($events as $event)
                                            <option
                                                value="{{$event->id.'|App\Models\Event|event|Etkinlikler -'.$event->title}}">Etkinlikler - {{$event->title}}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Sayfalar">
                                        @foreach($pages as $page)
                                            <option
                                                value="{{$page->id.'|App\Models\StaticContent|static|Sayfalar -'.$page->title}}">Sayfalar - {{$page->title}}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Mekan Kategoriler">
                                        @foreach($place_categories as $place_category)
                                            <option
                                                value="{{$place_category->id.'|App\Models\VideoCategory|place|Mekan Kategoriler - '.$place_category->title}}">Mekan Kategoriler - {{$place_category->title}}</option>
                                        @endforeach
                                    </optgroup>
                                    <optgroup label="Video Kategorileri">
                                        @foreach($video_categories as $video_category)
                                            <option
                                                value="{{$video_category->id.'|App\Models\VideoCategory|video|Video Kategorileri - '.$video_category->title}}">Video Kategorileri - {{$video_category->title}}</option>
                                        @endforeach
                                    </optgroup>
                                </select>
                                @if ($errors->has('row'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('row') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('image_path') ? ' has-error' : '' }}">
                                <label class="control-label">Görsel</label>
                                <div class="input-group input-file" name="image_path" style="width: 50%">
                                    <input type="text" class="form-control" placeholder='Dosya seçiniz...'/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-choose" type="button">Seç</button>
                                    </span>
                                </div>
                                @if ($errors->has('image_path'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('image_path') }}</strong>
                                    </span>
                                @endif
                            </div>


                        </div>
                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <script src="{{asset('assets/global/plugins/select2/js/select2.full.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN PAGE LEVEL SCRIPTS -->
    <script src="{{asset('assets/pages/scripts/components-select2.min.js')}}" type="text/javascript"></script>
    <!-- END PAGE LEVEL SCRIPTS -->
    <script>


        $(document).ready(function () {
            bs_input_file();
        });

        function bs_input_file() {
            $(".input-file").before(
                function () {
                    if (!$(this).prev().hasClass('input-ghost')) {
                        let element = $("<input type='file' class='input-ghost' accept='image/jpeg,image/jpg,image/png' style='visibility:hidden; height:0'>");
                        element.attr("name", $(this).attr("name"));
                        element.change(function () {
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function () {
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function () {
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor", "pointer");
                        $(this).find('input').mousedown(function () {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
    </script>
@endsection
