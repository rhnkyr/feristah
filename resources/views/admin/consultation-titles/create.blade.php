@extends('layouts.app')
@section('page_extra_css')
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.css" rel="stylesheet"
          type="text/css"/>
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Danısmanlık Başlık
                    <small>Ekle</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <form role="form" action="{{route('admin.consultation-titles.store')}}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="form-body">
                            <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                                <label class="control-label">Başlık</label>
                                <input type="text" class="form-control input-lg" style="width: 50%" name="title" value="{{old('title')}}"/>
                                @if ($errors->has('title'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('description') ? ' has-error' : '' }}">
                                <label class="control-label">Açıklama</label>
                                <textarea id="summernote" name="description"
                                          class="form-control">{{old('description')}}</textarea>
                                @if ($errors->has('description'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('description') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('temp_cover_image') ? ' has-error' : '' }}">
                                <label class="control-label">Üst Görsel</label>
                                <div class="input-group input-file" name="temp_cover_image" style="width: 50%">
                                    <input type="text" class="form-control" placeholder='Dosya seçiniz...'/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-choose" type="button">Seç</button>
                                    </span>
                                </div>
                                @if ($errors->has('temp_cover_image'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('temp_cover_image') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div style="margin-bottom: 10px;font-weight: bold;font-size: 18px;"> ya da </div>
                            <div class="form-group{{ $errors->has('video_url') ? ' has-error' : '' }}">
                                <label class="control-label">Video Url</label>
                                <input type="text" class="form-control input-lg" name="video_url" value="{{old('video_url')}}"/>
                                @if ($errors->has('video_url'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('video_url') }}</strong>
                                    </span>
                                @endif
                            </div>

                            <div class="form-group{{ $errors->has('file') ? ' has-error' : '' }}">
                                <label class="control-label">PDF</label>
                                <div class="input-group input-file" name="file" style="width: 50%">
                                    <input type="text" class="form-control" placeholder='Dosya seçiniz...'/>
                                    <span class="input-group-btn">
                                        <button class="btn btn-default btn-choose" type="button">Seç</button>
                                    </span>
                                </div>
                                @if ($errors->has('file'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('file') }}</strong>
                                    </span>
                                @endif
                            </div>

                        </div>

                        <div class="form-actions">
                            <button type="submit" class="btn blue">Kaydet</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('page_extra_js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/lang/summernote-tr-TR.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#summernote').summernote(
                {
                    toolbar: [
                        ['style', ['style']],
                        ['font', ['bold', 'underline', 'clear']],
                        ['fontname', ['fontname']],
                        ['color', ['color']],
                        ['para', ['ul', 'ol', 'paragraph']],
                        ['table', ['table']],
                        ['insert', ['link', 'picture']],
                        ['view', ['fullscreen', 'codeview']],
                    ],
                    tabsize: 2,
                    height: 300,
                    lang: 'tr-TR'
                }
            );

            bs_input_file();
        });

        function bs_input_file() {
            $(".input-file").before(
                function () {
                    if (!$(this).prev().hasClass('input-ghost')) {
                        let element = $("<input type='file' class='input-ghost' style='visibility:hidden; height:0'>");
                        element.attr("name", $(this).attr("name"));
                        element.change(function () {
                            element.next(element).find('input').val((element.val()).split('\\').pop());
                        });
                        $(this).find("button.btn-choose").click(function () {
                            element.click();
                        });
                        $(this).find("button.btn-reset").click(function () {
                            element.val(null);
                            $(this).parents(".input-file").find('input').val('');
                        });
                        $(this).find('input').css("cursor", "pointer");
                        $(this).find('input').mousedown(function () {
                            $(this).parents('.input-file').prev().click();
                            return false;
                        });
                        return element;
                    }
                }
            );
        }
    </script>
@endsection

