@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/global/plugins/')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')


    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Yayınlar
                        @if($rows instanceof \Illuminate\Pagination\LengthAwarePaginator)
                            ({{$rows->total()}})
                        @endif
                        <small>Liste</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box">
                <div class="portlet-body">
                    <div class="alert alert-info">
                        <strong>Bilgi!</strong> Sürükle bırak ile sıralamayı değiştirebilirsiniz.
                    </div>
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th> Sıra</th>
                                <th> Başlık</th>
                                <th> Durum</th>
                                <th> İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $row)
                                <tr id="items-{{$row->id}}">
                                    <td class="align-middle"> {{$row->order}}</td>
                                    <td class="align-middle"> {{$row->title}}</td>
                                    <td>
                                        @if($row->status === 1)
                                            <span class="label label-sm label-success"> Aktif </span>
                                        @else
                                            <span class="label label-sm label-warning"> Pasif </span>
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{route('admin.publishes.edit', ['id' => $row->id])}}"
                                           class="btn btn-xs btn-info">Güncelle</a>
                                        <a href="#"
                                           data-url="{{route('admin.publishes.destroy', ['id' => $row->id])}}"
                                           class="btn btn-xs btn-danger">Sil</a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    @if($rows instanceof \Illuminate\Pagination\LengthAwarePaginator)
                        <div class="row">
                            <div class="col-md-12">
                                {{ $rows->links() }}
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection

@section('page_extra_js')
    @if(session('success'))
        @include('layouts.partial.toastr')
    @endif
    @include('layouts.partial.sweet-alert')
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('tbody').sortable({
            update: function (event, ui) {
                let data = $(this).sortable('serialize');

                // POST to server using $.post or $.ajax
                $.ajax({
                    url: '{{route('admin.publishes.sort')}}',
                    type: 'POST',
                    data: data,
                    success: function (result) {
                        if (result.status) {
                            location.reload();
                        }
                    }
                });
            }
        });
    </script>
@endsection
