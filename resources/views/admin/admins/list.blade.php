@extends('layouts.app')
@section('page_extra_css')
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="{{asset('assets/global/plugins/bootstrap-toastr/toastr.min.css')}}" rel="stylesheet" type="text/css"/>
    <!-- END PAGE LEVEL PLUGINS -->
@endsection
@section('content')


    <!-- BEGIN PAGE BASE CONTENT -->
    <div class="row">

        <div class="col-md-12">
            <!-- BEGIN PAGE HEAD-->
            <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                    <h1>Yöneticiler
                        @if($rows instanceof \Illuminate\Pagination\LengthAwarePaginator)
                            ({{$rows->total()}})
                        @endif
                        <small>Liste</small>
                    </h1>
                </div>
                <!-- END PAGE TITLE -->
            </div>
            <!-- END PAGE HEAD-->
            <!-- BEGIN SAMPLE TABLE PORTLET-->
            <div class="portlet box">
                <div class="portlet-body">
                    <div class="table-scrollable">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th> Kullanıcı Adı</th>
                                <th> İşlem</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($rows as $row)
                                <tr>
                                    <td class="align-middle"> {{$row->user_name}}</td>
                                    <td>
                                        @if($row->user_name !== 'admin')
                                            <a href="#"
                                               data-url="{{route('admin.admins.destroy', ['id' => $row->id])}}"
                                               class="btn btn-xs btn-danger">Sil</a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- END SAMPLE TABLE PORTLET-->
        </div>
    </div>
    <!-- END PAGE BASE CONTENT -->
@endsection

@section('page_extra_js')
    @if(session('success'))
        @include('layouts.partial.toastr')
    @endif
    @include('layouts.partial.sweet-alert')
@endsection
