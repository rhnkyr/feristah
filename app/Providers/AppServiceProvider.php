<?php

    namespace App\Providers;

    use App\Models\Recommendation;
    use App\Models\User;
    use App\Models\UserFavorite;
    use App\Observers\RecommendationObserver;
    use App\Observers\UserObserver;
    use Illuminate\Support\ServiceProvider;

    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Register any application services.
         *
         * @return void
         */
        public function register()
        {
            if ($this->app->environment() === 'local') {
                $this->app->register(\Reliese\Coders\CodersServiceProvider::class);
            }
        }

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            User::observe(UserObserver::class);
            Recommendation::observe(RecommendationObserver::class);
        }
    }
