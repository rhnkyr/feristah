<?php


    namespace App\Aspects;


    use App\Models\Doctor;
    use Illuminate\Support\Collection;
    use Spatie\Searchable\SearchAspect;

    class DoctorSearchAspect extends SearchAspect
    {

        public static $searchType = 'doctors';

        public function getResults(string $term): Collection
        {
            return Doctor::with('hospital')
                ->where('name', 'LIKE', "%{$term}%")
                ->get();

        }
    }
