<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\ResourceCollection;

    class HomeElementsCollection extends ResourceCollection
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            if ($this->collection->count() === 1) {
                return [
                    'firstRow' => HomeElementsResource::collection($this->collection->first()),
                ];
            }

            return [
                'firstRow'  => HomeElementsResource::collection($this->collection->last()),
                'secondRow' => HomeElementsResource::collection($this->collection->first()),
            ];
        }
    }
