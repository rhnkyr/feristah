<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class UserResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'identifier'     => $this->identifier,
                'user_name'      => $this->user_name,
                'password_reset' => $this->password_reset,
            ];
        }
    }
