<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class ReminderResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id'          => $this->id,
                'title'       => $this->title,
                'description' => $this->description,
                'remind_at'   => $this->remind_at->format('d-m-Y'),
                'has_image'   => $this->has_image,
                'images'      => $this->whenLoaded('images'),
            ];
        }
    }
