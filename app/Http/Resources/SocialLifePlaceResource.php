<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class SocialLifePlaceResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id'        => $this->id,
                'icon'      => $this->category->icon_path,
                'title'     => $this->name,
                'sub_title' => $this->sub_title,
            ];
        }
    }
