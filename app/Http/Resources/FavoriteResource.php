<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class FavoriteResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id'           => $this->favoriteable->id,
                'name'         => $this->favoriteable->title ?? $this->favoriteable->name,
                'sub_title'    => $this->favoriteable->sub_title,
                'content_path' => $this->favoriteable->content_path,
                //'hospitals' => $this->hospitals,
                //'doctors'   => $this->doctors,
                //'videos'    => $this->videos,
                //'places'    => $this->places,
            ];
        }
    }
