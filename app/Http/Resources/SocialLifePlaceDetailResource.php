<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class SocialLifePlaceDetailResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'name'      => $this->name,
                'sub_title' => $this->sub_title,
                'address'   => $this->address,
                'location'  => $this->location,
            ];
        }
    }
