<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class HomeElementsResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'type'       => $this->data,
                'id'         => $this->content_id,
                'order'      => $this->order,
                //'title'      => $this->content->name ?? $this->content->title,
                'image_path' => $this->image_path,
            ];
        }
    }
