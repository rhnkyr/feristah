<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class DoctorResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id'         => $this->id,
                'profession' => $this->profession->title,
                'hospital'   => $this->hospital->title ?? '',
                'name'       => $this->name,
                'sub_title'  => $this->sub_title,
                'table_name' => $this->table_name,
            ];
        }
    }
