<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class HospitalResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id'         => $this->id,
                'name'       => $this->title,
                'sub_title'  => $this->sub_title,
                'address'    => $this->address,
                'location'   => $this->location,
                'table_name' => $this->table_name,
            ];
        }
    }
