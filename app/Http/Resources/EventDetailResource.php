<?php

    namespace App\Http\Resources;

    use Illuminate\Http\Resources\Json\JsonResource;

    class EventDetailResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function toArray($request)
        {
            return [
                'title'       => $this->title,
                'image_path'  => $this->image_path,
                'video_url'   => $this->video_url,
                'description' => $this->description,
                'when'        => $this->when->format('d.m.Y'),
            ];
        }
    }
