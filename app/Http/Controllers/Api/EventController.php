<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\EventDetailResource;
    use App\Http\Resources\EventResource;
    use App\Models\Event;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class EventController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $events = app(Event::class)
                ->orderBy('when', 'desc')
                ->active()
                ->get();

            return EventResource::collection($events);
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\EventDetailResource|array|\Illuminate\Http\Response
         */
        public function show($id)
        {
            try {

                $event = app(Event::class)
                    ->active()
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return new EventDetailResource($event);
        }

    }
