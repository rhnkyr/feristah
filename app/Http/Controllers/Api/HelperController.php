<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\PopularSearchResource;

    class HelperController extends Controller
    {
        public function popularSearch()
        {
            //$popular = app(PopularSearch::class)->orderBy('hit', 'desc')->limit(5)->get();

            $popular = collect((object)[
                (object)['keyword' => 'Cinsiyet Uyum Süreci', 'hit' => 1],
                (object)['keyword' => 'Hastane Prosedürü', 'hit' => 1],
                (object)['keyword' => 'Hukuki Prosedür', 'hit' => 1],
            ]);

            //dd($popular);

            return PopularSearchResource::collection($popular);

        }
    }
