<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Mail\ForgetPasswordMail;
    use App\Models\User;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class ForgetController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         * @throws \Exception
         */
        public function reset(Request $request)
        {
            if ($request->has('email') && $request->email !== '') {
                try {
                    $user = app(User::class)->whereEmail($request->email)->firstOrFail();

                    $password = random_int(100000, 999999);

                    $user->password = bcrypt($password);

                    $user->password_reset = 1;
                    $user->save();

                    \Mail::to($user->email)->send(new ForgetPasswordMail($password));

                    return [
                        'status'  => true,
                        'message' => 'Geçiçi şifreniz girmiş olduğunuz email adresine gönderildi!',
                    ];

                } catch (ModelNotFoundException $exception) {
                    return ['status' => false, 'message' => 'Girmiş olduğunuz email sistemimizde bulunamadı!'];
                }
            }

            return ['status' => false, 'message' => 'Girmiş olduğunuz email sistemimizde bulunamadı!'];
        }



        public function render(){
            return new ForgetPasswordMail('123456');
        }
    }


