<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\HomeElementsCollection;
    use App\Models\HomeScreenElement;

    class HomeElementController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \App\Http\Resources\HomeElementsCollection
         */
        public function index()
        {
            $elements = app(HomeScreenElement::class)->with('content')->get();
            //$elements = $elements->sortBy('order');

            $elements = $elements->groupBy('row');

            $elements = $elements->sortBy('order');

            return new HomeElementsCollection($elements);
        }
    }
