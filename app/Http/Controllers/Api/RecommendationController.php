<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\RecommendationRequest;
    use App\Models\Recommendation;
    use Illuminate\Database\QueryException;

    class RecommendationController extends Controller
    {


        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\RecommendationRequest $request
         *
         * @return array
         */
        public function store(RecommendationRequest $request)
        {

            try {

                app(Recommendation::class)->create($request->all());

            } catch (QueryException $exception) {

                return ['status' => false, 'message' => $exception->getMessage()];

            }

            return ['status' => true];

        }

    }
