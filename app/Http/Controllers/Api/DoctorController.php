<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\DoctorResource;
    use App\Models\Doctor;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class DoctorController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
        {
            $doctors = app(Doctor::class)
                ->with('hospital', 'profession')
                ->byCity(\request('city_id'))
                ->active()
                ->get();

            return DoctorResource::collection($doctors);
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\DoctorResource|array
         */
        public function show($id)
        {
            try {

                $doctor = app(Doctor::class)->active()->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return new DoctorResource($doctor);
        }
    }
