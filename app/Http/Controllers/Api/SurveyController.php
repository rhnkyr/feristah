<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Models\SurveyAnswer;
    use Illuminate\Http\Request;

    class SurveyController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return array
         */
        public function index()
        {
            $data = [
                [
                    'id'       => 1,
                    'type'     => 'dropdown',
                    'question' => 'Kendinizi Nasıl Tanımlıyorsunuz?',
                    'options'  => [
                        '1' => 'Trans',
                        '2' => 'Transseksüel Erkek',
                        '3' => 'Transseksüel Kadın',
                        '4' => 'Diğer (lütfen belirtiniz)',
                    ],
                ],
                [
                    'id'       => 2,
                    'type'     => 'dropdown',
                    'question' => 'Cinsel Yöneliminiz?',
                    'options'  => [
                        '1' => 'Lezbiyen',
                        '2' => 'Gay',
                        '3' => 'Biseksüel',
                        '4' => 'Heteroseksüel',
                        '5' => 'Panseksüel',
                        '6' => 'Aseksüel',
                        '7' => 'Diğer (lütfen belirtiniz)',
                    ],
                ],
                [
                    'id'       => 3,
                    'type'     => 'dropdown',
                    'question' => 'Yaşınız?',
                    'options'  => [
                        '1' => '-18',
                        '2' => '18-25',
                        '3' => '26-35',
                        '4' => '36-45',
                        '5' => '46+',
                    ],
                ],
                [
                    'id'       => 4,
                    'type'     => 'textbox',
                    'question' => 'Doğduğunuz Şehir?',
                ],
                [
                    'id'       => 5,
                    'type'     => 'textbox',
                    'question' => 'Şu Anda Yaşadığınız Şehir?',
                ],
                [
                    'id'       => 6,
                    'type'     => 'textbox',
                    'question' => 'Şimdiye Kadar Yaşadığınız Şehirler?',
                ],
                [
                    'id'       => 7,
                    'type'     => 'dropdown',
                    'question' => 'Medeni Durumunuz?',
                    'options'  => [
                        '1' => 'Bekar',
                        '2' => 'Evli',
                        '3' => 'Boşandım',
                    ],
                ],
                [
                    'id'       => 8,
                    'type'     => 'radio',
                    'question' => 'Çocuğunuz Var Mı?',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır',
                    ],
                ],
                [
                    'id'       => 9,
                    'type'     => 'dropdown',
                    'question' => 'Öğrenim Durumunuz?',
                    'options'  => [
                        '1' => 'İlkokul',
                        '2' => 'Ortaokul',
                        '3' => 'Lise',
                        '4' => 'Üniversite',
                        '5' => 'Yüksek lisans',
                    ],
                ],
                [
                    'id'       => 10,
                    'type'     => 'radio',
                    'question' => 'Çalışıyor musunuz? (cevabınız hayır ise lütfen 12.soruya geçiniz)',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır',
                    ],
                ],
                [
                    'id'       => 11,
                    'type'     => 'textbox',
                    'question' => 'Hangi işi yapıyorsunuz?',
                ],
                [
                    'id'       => 12,
                    'type'     => 'radio',
                    'question' => 'Aileniz ile Görüşüyor musunuz? ( cevabınız evet ise lütfen 14.soruya geçin)',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır',
                    ],
                ],
                [
                    'id'       => 13,
                    'type'     => 'textarea',
                    'question' => 'Ailenizle neden görüşmüyorsunuz?',
                ],
                [
                    'id'       => 14,
                    'type'     => 'dropdown',
                    'question' => 'Cinsiyet Uyum Sürecinize Başladınız Mı? ( Cevabınız Hayır içerikli ise lütfen 18.soruya geçiniz)',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır. Başlamayı düşünmüyorum',
                        '3' => 'Hayır. Başlamayı <u>düşünüyorum</u>',
                        '4' => 'Hayır. Kararsızım',
                    ],
                ],
                [
                    'id'       => 15,
                    'type'     => 'radio',
                    'question' => 'Cinsiyet Uyum Sürecinize Yasalarda Belirlenmiş Adımları Takip Ederek Mi Başladınız? ( cevabınız evet ise lütfen 17.soruya geçiniz)',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır',
                    ],
                ],
                [
                    'id'       => 16,
                    'type'     => 'textarea',
                    'question' => 'Cinsiyet Uyum Sürecinize Nasıl Başladığınızı Kısaca Anlatabilir misiniz?',
                ],
                [
                    'id'       => 17,
                    'type'     => 'dropdown',
                    'question' => 'Cinsiyet Uyum Sürecinizin Hangi Aşamasındasınız??',
                    'options'  => [
                        '1' => 'Psikiyatri görüşmesi yapıyorum',
                        '2' => 'Hormon kullanıyorum',
                        '3' => 'Ameliyat izin davası için hastaneden gerekli raporumu aldım',
                        '4' => 'Cinsiyet uyum süreci operasyonlarını olacağım',
                        '5' => 'Cinsiyet uyum süreci operasyonlarını oldum',
                        '6' => 'Kimlikteki cinsiyet hanesini değiştirdim, kimliğimi aldım.',
                    ],
                ],
                [
                    'id'       => 18,
                    'type'     => 'radio',
                    'question' => 'Doktor Kontrolü Dışında Hormon Kullandınız mı?',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır',
                    ],
                ],
                [
                    'id'       => 19,
                    'type'     => 'radio',
                    'question' => 'Kimlik değişimi için ameliyat olmak zorunda olmasaydınız cinsiyet uyum operasyonlarından olmayı tercih etmeyeceğiniz bir ameliyat olur muydu? (cevabınız hayır ise lütfen 21.soruya geçiniz)?',
                    'options'  => [
                        '1' => 'Evet',
                        '2' => 'Hayır',
                    ],
                    [
                        'id'       => 20,
                        'type'     => 'textarea',
                        'question' => 'Olmayı Tercih Etmeyeceğiniz Ameliyatlar Hangileri Olurdu?',
                    ],
                    [
                        'id'       => 21,
                        'type'     => 'textbox',
                        'question' => 'Feriştah uygulamasını Nereden Duydunuz?',
                    ],
                    [
                        'id'       => 22,
                        'type'     => 'textbox',
                        'question' => 'Feriştah\'ın İçindeki Bilgiler Sürecinize Yardımcı Oldu mu?',
                    ],
                ],
            ];

            return $data;
        }


        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function store(Request $request)
        {
            $result = json_decode($request->getContent());

            //dd(json_decode($result));

            foreach ($result as $r) {
                app(SurveyAnswer::class)->create([
                    'user_id'     => auth()->id(),
                    'question_id' => $r->question_id,
                    'answer'      => $r->answer,
                ]);
            }

            return ['status' => true];
        }
    }
