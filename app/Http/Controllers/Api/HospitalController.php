<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\HospitalResource;
    use App\Models\Hospital;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class HospitalController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $hospitals = app(Hospital::class)
                ->byCity(\request('city_id'))
                ->active()
                ->get();

            return HospitalResource::collection($hospitals);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\HospitalResource|array
         */
        public function show($id)
        {
            try {

                $hospital = app(Hospital::class)
                    ->active()
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return new HospitalResource($hospital);
        }

    }
