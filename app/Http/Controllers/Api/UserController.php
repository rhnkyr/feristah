<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\UserUpdateRequest;
    use App\Http\Resources\UserResource;

    class UserController extends Controller
    {

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Controllers\Api\UserResource|\App\Http\Resources\UserResource|\Illuminate\Http\Response
         */
        public function show($id)
        {
            $user = auth()->user();

            return new UserResource($user);
        }


        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\UserUpdateRequest $request
         * @param int                                  $id
         *
         * @return array
         */
        public function update(UserUpdateRequest $request, $id)
        {
            $user = auth()->user();

            if (!$user) {
                return ['status' => false, 'message' => 'Kullanıcı bulunamadı!'];
            }

            $user->user_name = $request->user_name;
            $user->password  = \Hash::make($request->password);
            $user->password_reset = 0;
            $user->save();

            return ['status' => true];

        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //todo : neler olacak?
        }
    }
