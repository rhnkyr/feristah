<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\SocialLifeCategoryResource;
    use App\Models\SocialLifeCategory;

    class SocialLifeCategoryController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $categories = app(SocialLifeCategory::class)
                ->orderBy('order')
                ->active()
                ->get();

            return SocialLifeCategoryResource::collection($categories);
        }
    }
