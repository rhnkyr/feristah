<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\ConsultationTitleResource;
    use App\Models\ConsultationTitle;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class ConsultationTitleController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $cts = app(ConsultationTitle::class)
                ->orderBy('title')
                ->get();

            return ConsultationTitleResource::collection($cts);
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return array
         */
        public function show($id)
        {
            try {

                $ct = app(ConsultationTitle::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return $ct;

        }

    }
