<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\VideoResource;
    use App\Models\Video;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class VideoController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $videos = app(Video::class)
                ->byCategory(\request('category_id'))
                ->active()
                ->get();


            return VideoResource::collection($videos);
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\VideoResource|array
         */
        public function show($id)
        {

            try {

                $video = app(Video::class)
                    ->with('consult')
                    ->active()
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {

                return ['status' => false, 'message' => $exception->getMessage()];

            }

            return new VideoResource($video);

        }

    }
