<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\CitiesResource;
    use App\Models\City;

    class CityController extends Controller
    {
        public function index()
        {
            $cities = app(City::class)->get();

            return CitiesResource::collection($cities);

        }
    }
