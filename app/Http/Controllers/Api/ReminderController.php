<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\ReminderResource;
    use App\Models\Reminder;
    use Exception;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class ReminderController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $reminders = auth()->user()->reminders;

            return ReminderResource::collection($reminders);
        }


        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return \App\Http\Resources\ReminderResource|\Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            try {

                $reminder = auth()->user()->reminders()->create($request->except('file'));

                if ((int)$request->has_image === 1) {

                    $name = \Str::uuid() . '.jpg';

                    $file = $request->file('file');

                    $photo_thumb = \Image::make($file)
                        ->fit(
                            165, 110, function ($constraint) {
                            $constraint->upsize();
                        })->encode('jpg', 80);

                    $photo_thumb_resource = $photo_thumb->stream()->detach();

                    $photo = \Image::make($file)
                        ->fit(
                            800, 600, function ($constraint) {
                            $constraint->upsize();
                        })->encode('jpg', 80);

                    $photo_resource = $photo->stream()->detach();

                    \Storage::put('files/reminders/' . auth()->id() . '/thumb-' . $name, $photo_thumb_resource);
                    \Storage::put('files/reminders/' . auth()->id() . '/' . $name, $photo_resource);

                    $reminder->images()->create(
                        [
                            'path'   => 'files/reminders/' . auth()->id() . '/' . $name,
                            'status' => 1,
                        ]);
                }


                /*if ((int)$request->has_image === 1) {

                    $img = $request->image_code;

                    $image = str_replace(['data:image/jpeg;base64,', ' '], ['', '+'], $img);

                    $name = \Str::uuid() . '.jpg';

                    \Storage::put('files/reminders/' . auth()->id() . '/' . $name, base64_decode($image));

                    $file        = \Storage::path('files/reminders/' . auth()->id() . '/' . $name);
                    $photo_thumb = \Image::make($file)
                        ->fit(165, 110, function ($constraint) {
                            $constraint->upsize();
                        })->encode('jpg', 80);

                    $photo_thumb_resource = $photo_thumb->stream()->detach();

                    $photo = \Image::make($file)
                        ->fit(800, 600, function ($constraint) {
                            $constraint->upsize();
                        })->encode('jpg', 80);

                    $photo_resource = $photo->stream()->detach();

                    \Storage::put('files/reminders/' . auth()->id() . '/thumb-' . $name, $photo_thumb_resource);
                    \Storage::put('files/reminders/' . auth()->id() . '/' . $name, $photo_resource);

                    $reminder->images()->create([
                        'path'   => 'files/reminders/' . auth()->id() . '/' . $name,
                        'status' => 1,
                    ]);
                }*/

                return new ReminderResource($reminder);
            } catch (Exception $e) {
                \Log::error($e->getMessage());
            }
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\ReminderResource|array
         */
        public function show($id)
        {

            try {
                $reminder = app(Reminder::class)
                    ->with('images')
                    ->where('user_id', auth()->id())
                    ->findOrFail($id);
            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return new ReminderResource($reminder);
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         * @throws \Exception
         */
        public function destroy($id)
        {
            try {
                $reminder = app(Reminder::class)
                    ->where('user_id', auth()->id())
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $reminder->delete();

            return ['status' => true, 'message' => 'İşleminiz gerçekleştirildi!'];
        }
    }
