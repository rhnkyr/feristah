<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\NotificationResource;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class NotificationController extends Controller
    {
        public function index()
        {
            $notifications = auth()->user()->notifications()->latest()->get();

            return NotificationResource::collection($notifications);
        }

        public function destroy($id)
        {
            try {

                $notification = auth()->user()->notifications()->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false];
            }

            $notification->delete();

            return ['status' => true];

        }


    }
