<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Mail\ConsultationMailable;
    use App\Models\ConsultationForm;
    use Illuminate\Http\Request;

    class ConsultationFormController extends Controller
    {

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function store(Request $request)
        {
            $form = app(ConsultationForm::class)->create(
                [
                    'consultation_title_id' => $request->consultation_title_id,
                    'user_id'               => auth()->id(),
                    'message'               => $request->message,
                ]
            );

            \Mail::to(env('MAIL_TO_SEND'))->queue(new ConsultationMailable($form));

            return ['status' => true];

        }
    }
