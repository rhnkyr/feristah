<?php

    namespace App\Http\Controllers\Api;

    use App\Aspects\DoctorSearchAspect;
    use App\Http\Controllers\Controller;
    use App\Models\Event;
    use App\Models\Hospital;
    use App\Models\PopularSearch;
    use App\Models\Publish;
    use App\Models\SocialLifePlace;
    use App\Models\Video;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Spatie\Searchable\Search;

    class SearchController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return array
         */
        public function index()
        {

            $q = \request('query');

            $searchResults = (new Search())
                ->registerModel(Hospital::class, 'title')
                ->registerAspect(DoctorSearchAspect::class)
                ->registerModel(Publish::class, 'title')
                ->registerModel(Event::class, 'title')
                ->registerModel(SocialLifePlace::class, 'name')
                ->registerModel(Video::class, 'title')
                ->search($q);

            $result = [];

            foreach ($searchResults->groupByType() as $type => $modelSearchResults) {

                foreach ($modelSearchResults as $searchResult) {

                    $extra_data = '';

                    if ($searchResult->type === 'doctors') {

                        $extra_data = str_replace('--', '', $searchResult->searchable->sub_title);
                        if ($searchResult->searchable->hospital) {
                            $extra_data .= '|' . $searchResult->searchable->hospital->title;
                        }
                    }

                    $result [$type] [] = [
                        'category_id' => $searchResult->searchable->video_category_id ?? '',
                        'extra_data'  => $extra_data,
                        'type'        => $searchResult->type,
                        'title'       => $searchResult->title,
                        'id'          => $searchResult->url,
                    ];
                }
            }

            if (count($result) > 0) {

                $slug = \Str::slug($q);

                try {

                    $popular = app(PopularSearch::class)->where('slug', $slug)->firstOrFail();
                    ++$popular->hit;
                    $popular->save();

                } catch (ModelNotFoundException $exception) {

                    app(PopularSearch::class)->create([
                        'keyword' => $q,
                        'slug'    => $slug,
                        'hit'     => 0,
                    ]);

                }
            }

            return $result;
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return string
         */
        public function show($id)
        {
            $params = explode('|', $id);//$type|$id

            switch ($params[0]) {
                case 'doctors' :
                    return app(DoctorController::class)->show($params[1]);
                case 'hospitals' :
                    return app(HospitalController::class)->show($params[1]);
                case 'events' :
                    return app(EventController::class)->show($params[1]);
                case 'publishes' :
                    return app(PublishController::class)->show($params[1]);
                case 'videos' :
                    return app(Video::class)->show($params[1]);
                case 'social_life_places' :
                    return app(SocialLifePlace::class)->show($params[1]);
                default:
                    return response()->json('Aradığınız kelimeye ait bir içerik bulunamadı!', 404);
            }

        }
    }
