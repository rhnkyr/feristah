<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\PublishDetailResource;
    use App\Http\Resources\PublishResource;
    use App\Models\Publish;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class PublishController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $publishes = app(Publish::class)
                ->orderBy('order')
                ->active()
                ->get();

            return PublishResource::collection($publishes);
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\PublishDetailResource|array
         */
        public function show($id)
        {
            try {

                $publish = app(Publish::class)
                    ->active()
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return new PublishDetailResource($publish);
        }

    }
