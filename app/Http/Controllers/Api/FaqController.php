<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Resources\FaqResource;
    use App\Models\Faq;
    use App\Http\Controllers\Controller;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class FaqController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index(): \Illuminate\Http\Resources\Json\AnonymousResourceCollection
        {
            $faqs = app(Faq::class)
                ->active()
                ->order()
                ->get();

            return FaqResource::collection($faqs);
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\FaqResource|array
         */
        public function show($id)
        {
            try {

                $faq = app(Faq::class)->active()->findOrFail($id);

            } catch (ModelNotFoundException $exception) {

                return ['status' => false, 'message' => $exception->getMessage()];

            }

            return new FaqResource($faq);
        }

    }
