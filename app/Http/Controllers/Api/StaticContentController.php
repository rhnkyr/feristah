<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Models\StaticContent;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class StaticContentController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            //
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return array|\Illuminate\Http\Response
         */
        public function show($id)
        {

            try {

                $content = app(StaticContent::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return $content;
        }

    }
