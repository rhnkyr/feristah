<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\FavoriteResource;
    use App\Models\UserFavorite;
    use Illuminate\Http\Request;

    class UserFavoriteController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $favs = auth()->user()->favorites()->with('favoriteable')->get();

            return FavoriteResource::collection($favs);

        }


        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return array
         */
        public function store(Request $request)
        {
            $model = str_replace(' ', '', ucwords(str_replace('-', ' ', $request->section)));

            $content = app("App\\Models\\$model")->find($request->content_id);
            $content->addFavorite();

            return ['status' => true];
        }


        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {

            try {
                $model = ucwords(\request('section'));

                $content = app("App\\Models\\$model")->find($id);
                $content->removeFavorite();

                return ['status' => true];

            } catch (\Exception $exception) {
                return ['status' => false];
            }

        }
    }
