<?php

    namespace App\Http\Controllers\Api;

    use App\Http\Controllers\Controller;
    use App\Http\Resources\SocialLifePlaceDetailResource;
    use App\Http\Resources\SocialLifePlaceResource;
    use App\Models\SocialLifePlace;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class SocialLifePlaceController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
         */
        public function index()
        {
            $places = app(SocialLifePlace::class)
                ->with('category')
                ->byCategory(\request('category_id'))
                ->orderBy('name')
                ->active()
                ->get();

            return SocialLifePlaceResource::collection($places);


        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \App\Http\Resources\SocialLifePlaceDetailResource|array|\Illuminate\Http\Response
         */
        public function show($id)
        {
            try {

                $place = app(SocialLifePlace::class)
                    ->active()
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {

                return ['status' => false, 'message' => $exception->getMessage()];

            }

            return new SocialLifePlaceDetailResource($place);
        }

    }
