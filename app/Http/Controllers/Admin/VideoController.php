<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\VideoRequest;
    use App\Models\Video;
    use App\Models\VideoCategory;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class VideoController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            if (\request('video_name')) {
                $rows = app(Video::class)
                    ->with('category')
                    ->where('title', 'LIKE', '%' . \request('video_name') . '%')
                    ->orderBy('title')
                    ->get();
            } else {
                $rows = app(Video::class)
                    ->with('category')
                    ->orderBy('title')
                    ->paginate(env('PAGER'));
            }

            return view('admin.videos.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $categories = app(VideoCategory::class)
                ->active()
                ->orderBy('title')
                ->get(['id', 'title']);


            return view('admin.videos.create', compact('categories'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\VideoRequest $request
         *
         * @return void
         */
        public function store(VideoRequest $request)
        {
            $request->merge(['content_path' => 'videos/' . \Str::slug($request->title)]);

            app(Video::class)->create($request->all());

            return redirect()->route('admin.videos.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(Video::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.videos.index');
            }

            $categories = app(VideoCategory::class)
                ->active()
                ->orderBy('title')
                ->get(['id', 'title']);

            return view('admin.videos.edit', compact('data', 'categories'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\VideoRequest $request
         * @param int                             $id
         *
         * @return void
         */
        public function update(VideoRequest $request, $id)
        {
            try {

                $data = app(Video::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.videos.index');
            }

            $request->merge(['content_path' => 'videos/' . \Str::slug($request->title)]);



            $data->update($request->all());

            return redirect()->route('admin.videos.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array|\Illuminate\Http\Response
         */
        public function destroy($id)
        {
            try {

                $data = app(Video::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
