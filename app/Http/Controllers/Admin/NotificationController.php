<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\NotificationRequest;
    use App\Models\User;

    class NotificationController extends Controller
    {
        public function index()
        {
            return view('admin.notifications.create');

        }

        public function send(NotificationRequest $request)
        {
            $users = app(User::class)->all();

            /*$notification_users = $users->filter(static function ($user, $key) {
                return $user->one_signal_id !== null;
            });*/

            foreach ($users as $user) {
                $user->notifications()->create([
                    'title'   => $request->title,
                    'content' => $request->message,
                ]);
            }

            $headings       = [];
            $headings['en'] = (string)$request->title;
            $headings['tr'] = (string)$request->title;


            \OneSignal::setParam('headings', $headings)
                ->sendNotificationToAll(
                    $request->message
                );

            return redirect()->route('admin.notification')->with('success', true);
        }
    }
