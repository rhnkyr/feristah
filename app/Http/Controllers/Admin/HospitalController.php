<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\HospitalRequest;
    use App\Models\City;
    use App\Models\Hospital;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class HospitalController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {

            if (\request('hospital_name')) {
                $rows = app(Hospital::class)
                    ->with('city')
                    ->where('title', 'LIKE', '%' . \request('hospital_name') . '%')
                    ->orderBy('title')
                    ->get();
            } else {
                $rows = app(Hospital::class)
                    ->with('city')
                    ->orderBy('title')
                    ->paginate(env('PAGER'));
            }

            return view('admin.hospitals.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {

            $cities = app(City::class)->all();

            return view('admin.hospitals.create', compact('cities'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\HospitalRequest $request
         *
         * @return void
         */
        public function store(HospitalRequest $request)
        {

            $request->merge(['content_path' => 'hospitals/' . \Str::slug($request->title)]);

            app(Hospital::class)->create($request->all());

            return redirect()->route('admin.hospitals.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(Hospital::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.hospitals.index');
            }

            $cities = app(City::class)->all();

            return view('admin.hospitals.edit', compact('data', 'cities'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\HospitalRequest $request
         * @param int                                $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(HospitalRequest $request, $id)
        {
            try {

                $data = app(Hospital::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.hospitals.index');
            }

            $request->merge(['content_path' => 'hospitals/' . \Str::slug($request->title)]);

            $data->update($request->all());

            return redirect()->route('admin.hospitals.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(Hospital::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
