<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\ProfessionRequest;
    use App\Models\Profession;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class ProfessionController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {

            $rows = app(Profession::class)
                ->orderBy('title')
                ->paginate(env('PAGER'));

            return view('admin.professions.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.professions.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\ProfessionRequest $request
         *
         * @return void
         */
        public function store(ProfessionRequest $request)
        {
            app(Profession::class)->create($request->all());

            return redirect()->route('admin.professions.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {

            try {

                $data = app(Profession::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.professions.index');
            }

            return view('admin.professions.edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\ProfessionRequest $request
         * @param int                                  $id
         *
         * @return void
         */
        public function update(ProfessionRequest $request, $id)
        {
            try {

                $data = app(Profession::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.professions.index');
            }

            $data->update($request->all());

            return redirect()->route('admin.professions.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(Profession::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
