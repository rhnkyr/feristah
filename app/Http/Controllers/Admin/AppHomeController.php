<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Models\ConsultationTitle;
    use App\Models\Event;
    use App\Models\HomeScreenElement;
    use App\Models\SocialLifeCategory;
    use App\Models\StaticContent;
    use App\Models\VideoCategory;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Database\QueryException;
    use Illuminate\Http\Request;

    class AppHomeController extends Controller
    {
        public function index()
        {

            $first_row_elements = app(HomeScreenElement::class)
                ->with('content')
                ->where('row', 1)
                ->orderBy('order')
                ->get();

            $second_row_elements = app(HomeScreenElement::class)
                ->with('content')
                ->where('row', 2)
                ->orderBy('order')
                ->get();

            return view('admin.home.index', compact('first_row_elements', 'second_row_elements'));

        }

        public function create(Request $request)
        {

            $events           = app(Event::class)->get();
            $pages            = app(StaticContent::class)->get();
            $consultations    = app(ConsultationTitle::class)->get();
            $video_categories = app(VideoCategory::class)->get();
            $place_categories = app(SocialLifeCategory::class)->get();
            //$hospitals        = app(Hospital::class)->get();
            //$doctors          = app(Doctor::class)->get();

            return view('admin.home.create',
                compact('events', 'pages', 'consultations', 'video_categories', 'place_categories'/*, 'hospitals',
                    'doctors'*/));
        }

        public function store(Request $request)
        {
            try {

                $data = explode('|', $request->data);

                //Get file
                $file = $request->file('image_path');

                //Rename file
                $name = round(microtime(true) * 1000) . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/home', $request->file('image_path'), $name);

                //Get path of new file
                $path = \Storage::path('files/home/' . $name);

                $img = \Image::make($path);

                /*if ($request->row === 1) {
                    //Crop center resize
                    $img->fit(771, 750, static function ($constraint) {
                        $constraint->upsize();
                    },'top');
                } else {
//Crop center resize
                    $img->fit(624, 435, static function ($constraint) {
                        $constraint->upsize();
                    },'top');
                }*/

                //Saveimage
                $img->save($path);

                if (count($data) > 2) {
                    app(HomeScreenElement::class)->create(
                        [
                            'row'          => $request->row,
                            'order'        => 1,
                            'image_path'   => 'files/home/' . $name,
                            'content_type' => $data[1],
                            'content_id'   => $data[0],
                            'data'         => $data[2],
                            'title'        => $data[3],
                        ]
                    );
                } else {
                    app(HomeScreenElement::class)->create(
                        [
                            'row'          => $request->row,
                            'order'        => 1,
                            'image_path'   => 'files/home/' . $name,
                            'content_type' => '',
                            'content_id'   => 0,
                            'data'         => $data[0],
                            'title'        => $data[1],
                        ]
                    );
                }


            } catch (QueryException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            return redirect()->route('admin.home-elements.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(HomeScreenElement::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }

        public function sortFirstRow(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(HomeScreenElement::class)->where('row', 1)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }

        public function sortSecondRow(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(HomeScreenElement::class)->where('row', 2)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }
    }
