<?php

    namespace App\Http\Controllers\Admin;

    use App\Enums\UserType;
    use App\Http\Controllers\Controller;
    use App\Http\Requests\AdminRequest;
    use App\Models\Doctor;
    use App\Models\Hospital;
    use App\Models\User;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class AdminController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(User::class)
                ->admins()
                ->orderBy('user_name')
                ->paginate(env('PAGER'));

            return view('admin.admins.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.admins.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\AdminRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(AdminRequest $request)
        {
            $admin             = new User;
            $admin->type       = UserType::ADMIN;
            $admin->identifier = \Str::uuid();
            $admin->user_name  = $request->user_name;
            $admin->password   = bcrypt($request->password);
            $admin->email      = $request->user_name . '@feristahapp.com';

            $admin->save();

            return redirect()->route('admin.admins.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(User::class)
                    ->where('type', UserType::ADMIN)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.admins.index');
            }

            return view('admin.admins.edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\AdminRequest $request
         * @param int                           $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(AdminRequest $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(User::class)
                    ->where('type', UserType::ADMIN)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }

        public function inside()
        {
            $total_users     = app(User::class)->count();
            $total_doctors   = app(Doctor::class)->count();
            $total_hospitals = app(Hospital::class)->count();

            return view('admin.dashboard', compact('total_users', 'total_doctors', 'total_hospitals'));

        }
    }
