<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\DoctorRequest;
    use App\Models\City;
    use App\Models\Doctor;
    use App\Models\Hospital;
    use App\Models\Profession;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class DoctorController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            if (\request('doctor_name')) {
                $rows = app(Doctor::class)
                    ->with('city', 'profession')
                    ->where('name', 'LIKE', '%' . \request('doctor_name') . '%')
                    ->orderBy('name')
                    ->get();
            } else {
                $rows = app(Doctor::class)
                    ->with('city', 'profession')
                    ->orderBy('name')
                    ->paginate(env('PAGER'));
            }

            return view('admin.doctors.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            $professions = app(Profession::class)
                ->active()
                ->orderBy('title')
                ->get(['id', 'title']);

            $cities    = app(City::class)->get(['id', 'name']);
            $hospitals = app(Hospital::class)->active()->get(['id', 'title']);

            return view('admin.doctors.create', compact('professions', 'hospitals', 'cities'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\DoctorRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(DoctorRequest $request)
        {

            $request->merge(['content_path' => 'doctors/' . \Str::slug($request->name)]);

            app(Doctor::class)->create($request->all());

            return redirect()->route('admin.doctors.index')->with('success', true);
        }


        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(Doctor::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.doctors.index');
            }

            $professions = app(Profession::class)
                ->active()
                ->orderBy('title')
                ->get(['id', 'title']);

            $cities    = app(City::class)->get(['id', 'name']);
            $hospitals = app(Hospital::class)->active()->get(['id', 'title']);

            return view('admin.doctors.edit', compact('data', 'professions', 'hospitals', 'cities'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(DoctorRequest $request, $id)
        {

            try {

                $data = app(Doctor::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.doctors.index');
            }

            $request->merge(['content_path' => 'doctors/' . \Str::slug($request->name)]);


            $data->update($request->all());

            return redirect()->route('admin.doctors.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(Doctor::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
