<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\EventRequest;
    use App\Models\Event;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class EventController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            if (\request('event_name')) {
                $rows = app(Event::class)
                    ->where('title', 'LIKE', '%' . \request('event_name') . '%')
                    ->orderBy('when', 'desc')
                    ->get();
            } else {
                $rows = app(Event::class)
                    ->where('title', 'LIKE', '%' . \request('event_name') . '%')
                    ->orderBy('when', 'desc')
                    ->paginate(env('PAGER'));
            }

            return view('admin.events.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.events.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\EventRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(EventRequest $request)
        {

            //Get file
            $file = $request->file('event_image_file');

            //Rename file
            $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

            //Save file
            \Storage::putFileAs('files/events', $request->file('event_image_file'), $name);

            //Get path of new file
            $path = \Storage::path('files/events/' . $name);

            $img = \Image::make($path);

            //Crop center resize
            $img->fit(1200, 900, static function ($constraint) {
                $constraint->upsize();
            });

            //Saveimage
            $img->save($path);


            $request->merge(['image_path' => 'files/events/' . $name]);

            $request->merge(['content_path' => 'events/' . \Str::slug($request->title)]);

            app(Event::class)->create($request->all());

            return redirect()->route('admin.events.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(Event::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.events.index');
            }

            return view('admin.events.edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            try {

                $data = app(Event::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.events.index');
            }

            if ($request->hasFile('event_image_file')) {
                //Get file
                $file = $request->file('event_image_file');

                //Rename file
                $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/events', $request->file('event_image_file'), $name);

                //Get path of new file
                $path = \Storage::path('files/events/' . $name);

                $img = \Image::make($path);

                //Crop center resize
                $img->fit(1200, 900, static function ($constraint) {
                    $constraint->upsize();
                });

                //Saveimage
                $img->save($path);

                $request->merge(['image_path' => 'files/events/' . $name]);

            } else {

                $request->merge(['image_path' => $request->old_file]);

            }

            $request->merge(['content_path' => 'events/' . \Str::slug($request->title)]);

            $data->update($request->all());

            return redirect()->route('admin.events.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array|\Illuminate\Http\Response
         */
        public function destroy($id)
        {
            try {

                $data = app(Event::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
