<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\ConsultationTitleRequest;
    use App\Models\ConsultationTitle;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class ConsultationTitleController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(ConsultationTitle::class)
                ->orderBy('title')
                ->get();

            return view('admin.consultation-titles.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.consultation-titles.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\ConsultationTitleRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(ConsultationTitleRequest $request)
        {

            if($request->has('temp_cover_image')) {
                //Get file
                $file = $request->file('temp_cover_image');

                //Rename file
                $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();
                //Save file
                \Storage::putFileAs('files/consultation', $request->file('temp_cover_image'), $name);

                //Get path of new file
                $path = \Storage::path('files/consultation/' . $name);

                $img = \Image::make($path);

                //Crop center resize
                $img->fit(1200, 900, static function ($constraint) {
                    $constraint->upsize();
                });

                //Saveimage
                $img->save($path);

                $request->merge(['cover_image' => 'files/consultation/' . $name]);
            }
            $request->merge(['has_file' => 0]);

            if ($request->hasFile('file')) {
                //Get file
                $file = $request->file('file');

                //Rename file
                $file_name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/consultation', $request->file('file'), $file_name);


                $request->merge(['file_path' => 'files/consultation/' . $file_name]);
                $request->merge(['has_file' => 1]);
            }

            app(ConsultationTitle::class)->create($request->all());

            return redirect()->route('admin.consultation-titles.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(ConsultationTitle::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.consultation-titles.index');
            }

            return view('admin.consultation-titles.edit', compact('data'));

        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(ConsultationTitleRequest $request, $id)
        {

            try {

                $data = app(ConsultationTitle::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.publishes.index');
            }

            if ($request->hasFile('temp_cover_image')) {
                //Get file
                $file = $request->file('temp_cover_image');

                //Rename file
                $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();
                //Save file
                \Storage::putFileAs('files/consultation', $request->file('temp_cover_image'), $name);

                //Get path of new file
                $path = \Storage::path('files/consultation/' . $name);

                $img = \Image::make($path);

                //Crop center resize
                $img->fit(1200, 900, static function ($constraint) {
                    $constraint->upsize();
                });

                //Saveimage
                $img->save($path);

                $request->merge(['cover_image' => 'files/consultation/' . $name]);

            } else {
                $request->merge(['cover_image' => $request->ex_cover_image]);
            }

            $request->merge(['has_file' => 0]);

            if ($request->hasFile('file')) {
                //Get file
                $file = $request->file('file');

                //Rename file
                $file_name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/consultation', $request->file('file'), $file_name);


                $request->merge(['file_path' => 'files/consultation/' . $file_name]);
                $request->merge(['has_file' => 1]);

            } else {

                $request->merge(['file_path' => null]);

            }

            $data->update($request->all());

            return redirect()->route('admin.consultation-titles.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(ConsultationTitle::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }

        public function sort(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(ConsultationTitle::class)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }
    }
