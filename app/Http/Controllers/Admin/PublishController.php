<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\PublishRequest;
    use App\Http\Requests\PublishUpdateRequest;
    use App\Models\Publish;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class PublishController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(Publish::class)
                ->orderBy('order')
                ->get();

            return view('admin.publishes.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.publishes.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\PublishRequest $request
         *
         * @return void
         */
        public function store(PublishRequest $request)
        {
            //Get file
            $file = $request->file('content_file');

            //Rename file
            $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

            //Save file
            \Storage::putFileAs('files/publishes', $request->file('content_file'), $name);


            $request->merge(['file_path' => 'files/publishes/' . $name]);
            $request->merge(['content_path' => 'publishes/' . \Str::slug($request->title)]);

            app(Publish::class)->create($request->all());

            return redirect()->route('admin.publishes.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(Publish::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.publishes.index');
            }

            return view('admin.publishes.edit', compact('data'));

        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\PublishUpdateRequest $request
         * @param int                                     $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(PublishUpdateRequest $request, $id)
        {
            try {

                $data = app(Publish::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.publishes.index');
            }

            if ($request->hasFile('content_file')) {
                //Get file
                $file = $request->file('content_file');

                //Rename file
                $name = \Str::slug($request->title) . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/publishes', $request->file('content_file'), $name);


                $request->merge(['file_path' => 'files/publishes/' . $name]);

            } else {

                $request->merge(['file_path' => $request->old_file]);

            }


            $request->merge(['content_path' => 'publishes/' . \Str::slug($request->title)]);


            $data->update($request->all());

            return redirect()->route('admin.publishes.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(Publish::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }

        public function sort(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(Publish::class)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }
    }
