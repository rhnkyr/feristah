<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\FaqRequest;
    use App\Models\Faq;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class FaqController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(Faq::class)
                ->orderBy('order')
                ->get();

            return view('admin.faqs.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.faqs.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\FaqRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(FaqRequest $request)
        {
            app(Faq::class)
                ->create($request->all());

            return redirect()->route('admin.faqs.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(Faq::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.faqs.index');
            }

            return view('admin.faqs.edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\FaqRequest $request
         * @param int                           $id
         *
         * @return void
         */
        public function update(FaqRequest $request, $id)
        {
            try {

                $data = app(Faq::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.faqs.index');
            }

            $data->update($request->all());

            return redirect()->route('admin.faqs.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {

            try {

                $data = app(Faq::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }

        public function sort(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(Faq::class)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }
    }
