<?php

    namespace App\Http\Controllers\Admin;

    use App\Enums\UserType;
    use App\Http\Controllers\Controller;
    use Illuminate\Foundation\Auth\User;
    use Illuminate\Http\Request;

    class UserController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            if (\request('user_name')) {
                $rows = app(User::class)
                    ->where('type', UserType::APPUSER)
                    ->where('user_name', 'LIKE', '%' . \request('user_name') . '%')
                    ->orderBy('user_name')
                    ->get();
            } else {
                $rows = app(User::class)
                    ->where('type', UserType::APPUSER)
                    ->where('user_name', 'LIKE', '%' . \request('user_name') . '%')
                    ->orderBy('user_name')
                    ->paginate(env('PAGER'));
            }

            return view('admin.users.list', compact('rows'));
        }


        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
