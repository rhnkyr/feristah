<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Models\SocialLifeCategory;
    use App\Models\SocialLifePlace;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class SocialLifePlaceController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            if (\request('place_name')) {
                $rows = app(SocialLifePlace::class)
                    ->with('category')
                    ->where('name', 'LIKE', '%' . \request('place_name') . '%')
                    ->orderBy('name')
                    ->get();
            } else {
                $rows = app(SocialLifePlace::class)
                    ->with('category')
                    ->orderBy('name')
                    ->paginate(env('PAGER'));
            }

            return view('admin.social.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {

            $categories = app(SocialLifeCategory::class)
                ->active()
                ->orderBy('order')
                ->get();

            return view('admin.social.create', compact('categories'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $request->merge(['content_path' => 'places/' . \Str::slug($request->name)]);

            app(SocialLifePlace::class)->create($request->all());

            return redirect()->route('admin.social-life-places.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(SocialLifePlace::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.social-life-places.index');
            }

            $categories = app(SocialLifeCategory::class)
                ->active()
                ->orderBy('order')
                ->get();

            return view('admin.social.edit', compact('data', 'categories'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            try {

                $data = app(SocialLifePlace::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.social-life-places.index');
            }

            $request->merge(['content_path' => 'places/' . \Str::slug($request->name)]);

            $data->update($request->all());

            return redirect()->route('admin.social-life-places.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array|\Illuminate\Http\Response
         */
        public function destroy($id)
        {
            try {

                $data = app(SocialLifePlace::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
