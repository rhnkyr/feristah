<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\ContentRequest;
    use App\Models\StaticContent;
    use Illuminate\Database\Eloquent\ModelNotFoundException;

    class StaticContentController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(StaticContent::class)
                ->orderBy('title')
                ->get();

            return view('admin.static.list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.static.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\ContentRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(ContentRequest $request)
        {

            if($request->hasFile('image_file')) {
                //Get file
                $file = $request->file('image_file');

                //Rename file
                $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/content', $request->file('image_file'), $name);

                //Get path of new file
                $path = \Storage::path('files/content/' . $name);

                $img = \Image::make($path);

                //Crop center resize
                $img->fit(1242, 669, static function ($constraint) {
                    $constraint->upsize();
                },'top');

                //Saveimage
                $img->save($path);

                $request->merge(['image_path' => 'files/content/' . $name]);

            }

            $request->merge(['content_path' => 'content/' . \Str::slug($request->title)]);

            app(StaticContent::class)->create($request->all());

            return redirect()->route('admin.static-contents.index')->with('success', true);
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(StaticContent::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.static-contents.index');
            }

            return view('admin.static.edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Models\StaticContent $request
         * @param int                       $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(ContentRequest $request, $id)
        {
            try {

                $data = app(StaticContent::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.static-contents.index');
            }

            if($request->hasFile('image_file')) {
                //Get file
                $file = $request->file('image_file');

                //Rename file
                $name = \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/content', $request->file('image_file'), $name);

                //Get path of new file
                $path = \Storage::path('files/content/' . $name);

                $img = \Image::make($path);

                //Crop center resize
                $img->fit(1242, 669, static function ($constraint) {
                    $constraint->upsize();
                },'top');

                //Saveimage
                $img->save($path);

                $request->merge(['image_path' => 'files/content/' . $name]);

            }

            $request->merge(['content_path' => 'content/' . \Str::slug($request->title)]);

            $data->update($request->all());

            return redirect()->route('admin.static-contents.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array
         */
        public function destroy($id)
        {
            try {

                $data = app(StaticContent::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }
    }
