<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\SocialLifeCategoryRequest;
    use App\Models\SocialLifeCategory;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class SocialLifeCategoryController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(SocialLifeCategory::class)
                ->orderBy('order')
                ->get();

            return view('admin.social.category-list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.social.category-create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\SocialLifeCategoryRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(SocialLifeCategoryRequest $request)
        {
            //Get file
            $file = $request->file('image_path');

            //Rename file
            $name = 'image-' . \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

            //Save file
            \Storage::putFileAs('files/social-life', $file, $name);

            //Get path of new file
            $path = \Storage::path('files/social-life/' . $name);

            $img = \Image::make($path);

            //Crop center resize
            $img->fit(1200, 900, static function ($constraint) {
                $constraint->upsize();
            });

            //Saveimage
            $img->save($path);

            //Get file
            $file2 = $request->file('icon_path');

            //Rename file
            $name2 = 'icon-' . \Str::slug($request->title) . '-' . time() . '.' . $file2->extension();

            //Save file
            \Storage::putFileAs('files/social-life', $file2, $name2);

            $request = $request->all();

            $request['icon_path'] = 'files/social-life/' . $name2;

            $request['image_path'] = 'files/social-life/' . $name;

            app(SocialLifeCategory::class)->create($request);

            return redirect()->route('admin.social-life-categories.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(SocialLifeCategory::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.social-life-categories.index');
            }

            return view('admin.social.category-edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int                      $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            try {

                $data = app(SocialLifeCategory::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.social-life-categories.index');
            }

            if ($request->hasFile('image_path')) {

                $file = $request->file('image_path');

                //Rename file
                $name = 'image-' . \Str::slug($request->title) . '-' . time() . '.' . $file->extension();

                //Save file
                \Storage::putFileAs('files/social-life', $file, $name);

                //Get path of new file
                $path = \Storage::path('files/social-life/' . $name);

                $img = \Image::make($path);

                //Crop center resize
                $img->fit(1200, 900, static function ($constraint) {
                    $constraint->upsize();
                });

                //Saveimage
                $img->save($path);


                $image_path = 'files/social-life/' . $name;

            } else {

                $image_path = $request->old_image_path;

            }
            if ($request->hasFile('icon_path')) {
                //Get file
                $file2 = $request->file('icon_path');

                //Rename file
                $name2 = 'icon-' . \Str::slug($request->title) . '-' . time() . '.' . $file2->extension();

                //Save file
                \Storage::putFileAs('files/social-life', $file2, $name2);


                $icon_path = 'files/social-life/' . $name2;

            } else {

                $icon_path = $request->old_icon_path;

            }

            $request = $request->all();

            $request['icon_path'] = $icon_path;

            $request['image_path'] = $image_path;

            $data->update($request);

            return redirect()->route('admin.social-life-categories.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array|\Illuminate\Http\Response
         */
        public function destroy($id)
        {

            try {

                $data = app(SocialLifeCategory::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->places()->delete();

            $data->delete();

            session()->flash('success');

            return ['status' => true];


        }

        public function sort(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(SocialLifeCategory::class)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }
    }
