<?php

    namespace App\Http\Controllers\Admin;

    use App\Http\Controllers\Controller;
    use App\Http\Requests\VideoCategoryRequest;
    use App\Models\VideoCategory;
    use Illuminate\Database\Eloquent\ModelNotFoundException;
    use Illuminate\Http\Request;

    class VideoCategoryController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $rows = app(VideoCategory::class)
                ->orderBy('order')
                ->get();

            return view('admin.videos.category-list', compact('rows'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.videos.category-create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \App\Http\Requests\VideoCategoryRequest $request
         *
         * @return \Illuminate\Http\Response
         */
        public function store(VideoCategoryRequest $request)
        {
            app(VideoCategory::class)
                ->create($request->all());

            return redirect()->route('admin.video-categories.index')->with('success', true);
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         *
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            try {

                $data = app(VideoCategory::class)->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.video-categories.index');
            }

            return view('admin.videos.category-edit', compact('data'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \App\Http\Requests\VideoCategoryRequest $request
         * @param int                                     $id
         *
         * @return \Illuminate\Http\Response
         */
        public function update(VideoCategoryRequest $request, $id)
        {
            try {

                $data = app(VideoCategory::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return redirect()->route('admin.video-categories.index');
            }

            $data->update($request->all());

            return redirect()->route('admin.video-categories.index')->with('success', true);
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         *
         * @return array|\Illuminate\Http\Response
         */
        public function destroy($id)
        {
            try {

                $data = app(VideoCategory::class)
                    ->findOrFail($id);

            } catch (ModelNotFoundException $exception) {
                return ['status' => false, 'message' => $exception->getMessage()];
            }

            $data->videos()->delete();

            $data->delete();

            session()->flash('success');

            return ['status' => true];
        }

        public function sort(Request $request)
        {
            $i = 0;

            foreach ($request->items as $item) {

                $data        = app(VideoCategory::class)->find($item);
                $data->order = $i + 1;
                $data->save();

                $i++;
            }

            session()->flash('success');

            return ['status' => true];
        }
    }
