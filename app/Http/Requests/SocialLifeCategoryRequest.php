<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class SocialLifeCategoryRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return auth()->check();
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'      => 'required',
                'status'     => 'required',
                'image_path' => 'required|mimes:jpeg,png,jpg,svg|max:2048',
                'icon_path'  => 'required|mimes:jpeg,png,jpg,svg|max:2048',
            ];
        }
    }
