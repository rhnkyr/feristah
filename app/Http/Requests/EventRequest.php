<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class EventRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return auth()->check();
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'            => 'required',
                'description'      => 'required',
                'status'           => 'required',
                'when'             => 'required|date_format:d-m-Y H:i|after:today',
                'event_image_file' => 'required|mimes:jpeg,png,jpg|max:2048',
            ];
        }
    }
