<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class DoctorRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return auth()->check();
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'profession_id' => 'required',
                'city_id'       => 'required',
                'name'          => 'required',
                'sub_title'     => 'required',
                'status'        => 'required',
            ];
        }
    }
