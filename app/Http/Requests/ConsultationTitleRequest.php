<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class ConsultationTitleRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return auth()->check();
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'title'            => 'required',
                'description'      => 'required',
                'file'             => 'nullable|mimes:pdf',
                'temp_cover_image' => 'nullable|mimes:jpeg,jpg,png,svg',
                'video_url'        => 'nullable|url',
            ];
        }
    }
