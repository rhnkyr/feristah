<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class VideoRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return auth()->check();
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'video_category_id' => 'required|int',
                'title'             => 'required',
                'source'            => 'required|url',
                'description'       => 'required',
                'status'            => 'required|int',
            ];
        }
    }
