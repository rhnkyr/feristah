<?php

    namespace App\Mail;

    use App\Models\ConsultationForm;
    use Illuminate\Bus\Queueable;
    use Illuminate\Mail\Mailable;
    use Illuminate\Queue\SerializesModels;

    class ConsultationMailable extends Mailable
    {
        use Queueable, SerializesModels;

        private $form;

        /**
         * Create a new message instance.
         *
         * @param \App\Models\ConsultationForm $form
         */
        public function __construct(ConsultationForm $form)
        {
            $this->form = $form;
        }

        /**
         * Build the message.
         *
         * @return $this
         */
        public function build()
        {
            return $this->from('no-reply@mybikinicompetition.com', 'Feriştah Uygulama')
                ->subject('Danışmanlık Formu')
                ->view('mails.consult')
                ->with([
                    'form' => $this->form,
                ]);

        }
    }
