<?php

namespace App\Listeners;

use App\Events\ReminderMatched;
use Illuminate\Contracts\Queue\ShouldQueue;

class SendReminderNotification implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReminderMatched  $event
     * @return void
     */
    public function handle(ReminderMatched $event)
    {
        $message = env('NOTIFICATION_MESSAGE');

        //Send notification
        $headings = [];
        $headings['en'] = (string)$message;
        $headings['tr'] = (string)$message;

        \OneSignal::setParam('headings', $headings)
            ->sendNotificationToUser(
                $event->reminder->title, $event->reminder->user->one_signal_id, '', '', $buttons = null,
                $schedule = null);
    }
}
