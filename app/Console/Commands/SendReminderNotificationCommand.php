<?php

    namespace App\Console\Commands;

    use App\Events\ReminderMatched;
    use App\Models\Reminder;
    use Illuminate\Console\Command;

    class SendReminderNotificationCommand extends Command
    {
        /**
         * The name and signature of the console command.
         *
         * @var string
         */
        protected $signature = 'notification:reminders';

        /**
         * The console command description.
         *
         * @var string
         */
        protected $description = 'Send reminder notification for user';

        /**
         * Create a new command instance.
         *
         * @return void
         */
        public function __construct()
        {
            parent::__construct();
        }

        /**
         * Execute the console command.
         *
         * @return mixed
         */
        public function handle()
        {


            $reminder = app(Reminder::class)->with('user')->find(45);

            event(new ReminderMatched($reminder));

            return true;

            app(Reminder::class)->with('user')
                ->whereDate('remind_at', '<=', now())
                ->where('is_reminded', 0)
                ->get()
                ->each(
                    function (
                        Reminder $reminder
                    ) {
                        $this->sendReminderNotification($reminder);
                    });

            return true;
        }

        protected function sendReminderNotification(Reminder $reminder)
        {

            if ($reminder->user->one_signal_id !== null) {

                event(new ReminderMatched($reminder));

                //Update row
                $reminder->is_reminded = 1;
                $reminder->save();
            } else {
                $reminder->user->notifications()->create(
                    [
                        'title'   => $reminder->title,
                        'content' => $reminder->description,
                    ]);
            }

        }

    }
