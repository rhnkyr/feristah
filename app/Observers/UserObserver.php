<?php

    namespace App\Observers;

    use App\Enums\UserType;
    use App\Models\User;

    class UserObserver
    {
        /**
         * Handle the user "created" event.
         *
         * @param \App\Models\User $user
         *
         * @return void
         */
        public function created(User $user)
        {
            switch ($user->type) {
                case UserType::ADMIN :
                    $user->assignRole('admin');
                    break;
                case UserType::APPUSER :
                    $user->assignRole('application-user');
                    break;
            }

        }


        /**
         * Handle the user "deleted" event.
         *
         * @param \App\Models\User $user
         *
         * @return void
         */
        public function deleted(User $user)
        {
            switch ($user->type) {
                case UserType::ADMIN :
                    $user->removeRole('admin');
                    break;
                case UserType::APPUSER :
                    $user->removeRole('application_user');
                    break;
            }
        }
    }
