<?php

    namespace App\Observers;

    use App\Models\Recommendation;

    class RecommendationObserver
    {
        public function creating(Recommendation $recommendation)
        {
            $recommendation->user_id = auth()->id();
            $recommendation->is_read = 0;
        }
    }
