<?php

    namespace App\Observers;

    use App\Models\UserFavorite;

    class UserFavoriteObserver
    {
        /**
         * Handle the user "created" event.
         *
         * @param \App\Models\UserFavorite $userFavorite
         *
         * @return void
         */
        public function saving(UserFavorite $userFavorite)
        {
            $userFavorite->user_id = auth()->id();
        }
    }
