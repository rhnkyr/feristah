<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:47:52 +0000.
     */

    namespace App\Models;

    use Carbon\Carbon;
    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class Reminder
     *
     * @property int            $id
     * @property string         $title
     * @property string         $description
     * @property string         $image_path
     * @property int            $has_image
     * @property \Carbon\Carbon $remind_at
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Reminder extends Eloquent
    {
        protected $casts
            = [
                'has_image' => 'int',
            ];

        protected $dates
            = [
                'remind_at',
            ];

        protected $fillable
            = [
                'user_id',
                'title',
                'description',
                'has_image',
                'remind_at',
            ];


        public function setRemindAtAttribute($value)
        {
            $this->attributes['remind_at'] = Carbon::createFromFormat('Y-m-d', $value)->setTime(9,0,0);
        }

        public function images()
        {
            return $this->hasMany(ContentImage::class, 'content_id', 'id');
        }

        public function user()
        {
            return $this->belongsTo(User::class);
        }
    }
