<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:28:12 +0000.
     */

    namespace App\Models;

    use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
    use Reliese\Database\Eloquent\Model as Eloquent;
    use Spatie\Searchable\Searchable;
    use Spatie\Searchable\SearchResult;

    /**
     * Class Hospital
     *
     * @property int            $id
     * @property string         $title
     * @property string         $content_path
     * @property string         $sub_title
     * @property string         $address
     * @property string         $location
     * @property int            $status
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Hospital extends Eloquent implements Searchable
    {

        use Favoriteable;

        protected $appends
            = [
                'table_name',
            ];

        protected $casts
            = [
                'status' => 'int',
                'city_id' => 'int'
            ];

        protected $fillable
            = [
                'title',
                'city_id',
                'content_path',
                'sub_title',
                'address',
                'location',
                'status',
            ];

        public function doctors()
        {
            return $this->hasMany(Doctor::class);
        }

        public function city()
        {
            return $this->belongsTo(City::class);
        }

        public function scopeByCity($query, $city_id = 34)
        {
            if ($city_id !== 0) {
                return $query->where('city_id', $city_id);
            }

            return $query;
        }

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }

        public function getTableNameAttribute(): string
        {
            return $this->getTable();
        }

        public function getSearchResult(): SearchResult
        {
            return new SearchResult(
                $this,
                $this->title,
                $this->id
            );
        }
    }
