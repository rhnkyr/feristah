<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:46:41 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class Profession
     *
     * @property int            $id
     * @property string         $title
     * @property int            $status
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Profession extends Eloquent
    {
        protected $casts
            = [
                'status' => 'int',
            ];

        protected $fillable
            = [
                'title',
                'status',
            ];


        public function doctors()
        {
            return $this->hasMany(Doctor::class);
        }

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }
    }
