<?php

    /**
     * Created by Reliese Model.
     * Date: Thu, 03 Oct 2019 14:23:48 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class Notification
     *
     * @property int            $id
     * @property int            $user_id
     * @property string         $content
     * @property int            $status
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Notification extends Eloquent
    {
        protected $casts
            = [
                'user_id' => 'int',
                'status'  => 'int',
            ];

        protected $fillable
            = [
                'user_id',
                'title',
                'content',
                'status',
            ];
    }
