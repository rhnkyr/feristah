<?php

    /**
     * Created by Reliese Model.
     * Date: Sun, 12 May 2019 08:39:23 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class Recommendation
     *
     * @property int            $id
     * @property int            $user_id
     * @property string         $title
     * @property string         $content
     * @property int            $is_read
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Recommendation extends Eloquent
    {
        protected $casts
            = [
                'user_id' => 'int',
                'is_read' => 'int',
            ];

        protected $fillable
            = [
                'user_id',
                'title',
                'content',
                'is_read',
            ];

        public function user()
        {
            return $this->belongsTo(User::class);
        }
    }
