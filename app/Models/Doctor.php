<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:28:12 +0000.
     */

    namespace App\Models;

    use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
    use Reliese\Database\Eloquent\Model as Eloquent;
    use Spatie\Searchable\Searchable;
    use Spatie\Searchable\SearchResult;

    /**
     * Class Doctor
     *
     * @property int            $id
     * @property int            $profession_id
     * @property int            $hospital_id
     * @property int            $city_id
     * @property string         $name
     * @property string         $content_path
     * @property string         $sub_title
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Doctor extends Eloquent implements Searchable
    {

        use Favoriteable;

        protected $appends
            = [
                'table_name',
            ];

        protected $casts
            = [
                'profession_id' => 'int',
                'hospital_id'   => 'int',
                'city_id'       => 'int',
                'status'        => 'int',
            ];

        protected $fillable
            = [
                'profession_id',
                'hospital_id',
                'city_id',
                'name',
                'content_path',
                'sub_title',
                'status',
            ];


        public function hospital()
        {
            return $this->belongsTo(Hospital::class);
        }

        public function profession()
        {
            return $this->belongsTo(Profession::class);
        }

        public function city()
        {
            return $this->belongsTo(City::class);
        }

        public function scopeByCity($query, $city_id = 34)
        {
            if ($city_id !== 0) {
                return $query->where('city_id', $city_id);
            }

            return $query;
        }

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }

        public function getTableNameAttribute(): string
        {
            return $this->getTable();
        }

        public function getSearchResult(): SearchResult
        {

            return new SearchResult(
                $this,
                $this->name,
                $this->id
            );

        }
    }
