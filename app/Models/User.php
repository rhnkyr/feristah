<?php

    /**
     * Created by Reliese Model.
     * Date: Sun, 12 May 2019 08:25:25 +0000.
     */

    namespace App\Models;

    /**
     * Class User
     *
     * @property int            $id
     * @property string         $identifier
     * @property int            $type
     * @property string         $user_name
     * @property \Carbon\Carbon $birthday
     * @property string         $email
     * @property string         $city
     * @property string         $country
     * @property string         $hear_us
     * @property int            $is_kvk_accepted
     * @property \Carbon\Carbon $email_verified_at
     * @property string         $password
     * @property string         $remember_token
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */

    use App\Enums\UserType;
    use Carbon\Carbon;
    use ChristianKuri\LaravelFavorite\Traits\Favoriteability;
    use Illuminate\Foundation\Auth\User as Authenticatable;
    use Illuminate\Notifications\Notifiable;
    use Laravel\Passport\HasApiTokens;
    use Spatie\Permission\Traits\HasRoles;

    class User extends Authenticatable
    {
        use HasApiTokens, Notifiable, HasRoles, Favoriteability;

        protected $casts
            = [
                'type'            => 'int',
                'is_kvk_accepted' => 'int',
            ];

        protected $dates
            = [
                'birthday',
                'email_verified_at',
            ];

        protected $hidden
            = [
                'password',
                'remember_token',
            ];

        protected $fillable
            = [
                'identifier',
                'type',
                'user_name',
                'birthday',
                'email',
                'city',
                'country',
                'hear_us',
                'is_kvk_accepted',
                'email_verified_at',
                'password',
                'remember_token',
            ];

        public function setBirthdayAttribute($value)
        {
            $this->attributes['birthday'] = Carbon::createFromFormat('d-m-Y', $value);
        }


        public function findForPassport($username)
        {
            return $this->where('user_name', $username)->first();
        }


        public function recommendations()
        {
            return $this->hasMany(Recommendation::class);
        }

        public function reminders()
        {
            return $this->hasMany(Reminder::class);
        }

        public function notifications()
        {
            return $this->hasMany(Notification::class);
        }

        public function scopeAdmins($query)
        {
            return $query->where('type', UserType::ADMIN);
        }

        public function scopeApplicationUsers($query)
        {
            return $query->where('type', UserType::APPUSER);
        }

    }
