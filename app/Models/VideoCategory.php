<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:50:28 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class VideoCategory
     *
     * @property int            $id
     * @property string         $title
     * @property int            $status
     * @property int            $order
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class VideoCategory extends Eloquent
    {
        protected $casts
            = [
                'status' => 'int',
                'order' => 'int',
            ];

        protected $fillable
            = [
                'title',
                'status',
                'order',
            ];

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }
    }
