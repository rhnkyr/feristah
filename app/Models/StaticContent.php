<?php

/**
 * Created by Reliese Model.
 * Date: Sun, 15 Sep 2019 13:58:27 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StaticContent
 * 
 * @property int $id
 * @property string $title
 * @property string $content_path
 * @property string $image_path
 * @property string $video_url
 * @property string $description
 * @property int $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class StaticContent extends Eloquent
{
	protected $casts = [
		'status' => 'int'
	];

	protected $fillable = [
		'title',
		'content_path',
		'image_path',
		'video_url',
		'description',
		'status'
	];
}
