<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 May 2019 14:47:33 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;
use Spatie\Searchable\Searchable;
use Spatie\Searchable\SearchResult;

/**
 * Class Publish
 * 
 * @property int $id
 * @property string $title
 * @property string $content_path
 * @property string $description
 * @property string $file_path
 * @property int $status
 * @property int $order
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Publish extends Eloquent implements Searchable
{

	protected $casts = [
		'status' => 'int',
		'order' => 'int'
	];

	protected $fillable = [
		'title',
		'content_path',
		'description',
		'file_path',
		'status',
		'order'
	];

    public function scopeActive($query)
    {
        return $query->where('status', 1);
    }

    public function getSearchResult(): SearchResult
    {
        return new SearchResult(
            $this,
            $this->title,
            $this->id
        );
    }
}
