<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:49:35 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class SocialLifeCategory
     *
     * @property int            $id
     * @property string         $title
     * @property string         $icon_path
     * @property string         $image_path
     * @property int            $status
     * @property int            $order
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class SocialLifeCategory extends Eloquent
    {
        protected $casts
            = [
                'status' => 'int',
                'order'  => 'int',
            ];

        protected $fillable
            = [
                'title',
                'icon_path',
                'image_path',
                'status',
                'order',
            ];

        public function places()
        {
            return $this->hasMany(SocialLifePlace::class);
        }


        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }
    }
