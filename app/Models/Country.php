<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 03 Oct 2019 14:02:00 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 * 
 * @property int $country_id
 * @property string $country_name
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $primaryKey = 'country_id';
	public $timestamps = false;

	protected $fillable = [
		'country_name'
	];
}
