<?php

/**
 * Created by Reliese Model.
 * Date: Mon, 27 May 2019 14:24:37 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ContentImage
 * 
 * @property int $id
 * @property int $content_id
 * @property string $path
 * @property string $status
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ContentImage extends Eloquent
{
	protected $casts = [
		'content_id' => 'int'
	];

	protected $fillable = [
		'content_id',
		'path',
		'status'
	];

}
