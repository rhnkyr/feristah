<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:28:12 +0000.
     */

    namespace App\Models;

    use Carbon\Carbon;
    use Reliese\Database\Eloquent\Model as Eloquent;
    use Spatie\Searchable\Searchable;
    use Spatie\Searchable\SearchResult;

    /**
     * Class Event
     *
     * @property int            $id
     * @property string         $title
     * @property string         $content_path
     * @property string         $image_path
     * @property string         $description
     * @property int            $status
     * @property \Carbon\Carbon $when
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Event extends Eloquent implements Searchable
    {


        protected $casts
            = [
                'status' => 'int',
            ];

        protected $dates
            = [
                'when',
            ];

        protected $fillable
            = [
                'title',
                'content_path',
                'image_path',
                'video_url',
                'description',
                'status',
                'when',
            ];

        public function setWhenAttribute($value)
        {
            $this->attributes['when'] = Carbon::createFromFormat('d-m-Y H:i', $value);
        }

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }

        public function getSearchResult(): SearchResult
        {
            return new SearchResult(
                $this,
                $this->title,
                $this->id
            );
        }
    }
