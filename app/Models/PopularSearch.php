<?php

/**
 * Created by Reliese Model.
 * Date: Thu, 25 Jul 2019 15:27:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class PopularSearch
 * 
 * @property int $id
 * @property string $keyword
 * @property string $slug
 * @property int $hit
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class PopularSearch extends Eloquent
{
	protected $casts = [
		'hit' => 'int'
	];

	protected $fillable = [
		'keyword',
		'slug',
		'hit'
	];
}
