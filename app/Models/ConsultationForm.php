<?php

    /**
     * Created by Reliese Model.
     * Date: Sat, 27 Jul 2019 05:59:05 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class ConsultationForm
     *
     * @property int            $id
     * @property int            $consultation_title_id
     * @property int            $user_id
     * @property string         $message
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class ConsultationForm extends Eloquent
    {
        protected $casts
            = [
                'consultation_title_id' => 'int',
                'user_id'               => 'int',
            ];

        protected $fillable
            = [
                'consultation_title_id',
                'user_id',
                'message',
            ];

        public function user()
        {
            return $this->belongsTo(User::class);
        }

        public function consultation()
        {
            return $this->belongsTo(ConsultationTitle::class,'consultation_title_id','id');
        }
    }
