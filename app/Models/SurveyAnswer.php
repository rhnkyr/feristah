<?php

/**
 * Created by Reliese Model.
 * Date: Sat, 28 Sep 2019 09:34:57 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SurveyAnswer
 * 
 * @property int $id
 * @property int $user_id
 * @property int $question_id
 * @property string $answer
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SurveyAnswer extends Eloquent
{
	protected $casts = [
		'user_id' => 'int',
		'question_id' => 'int'
	];

	protected $fillable = [
		'user_id',
		'question_id',
		'answer'
	];
}
