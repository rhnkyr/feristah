<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 May 2019 14:28:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class ConsultationTitle
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $cover_image
 * @property int $has_file
 * @property string $file_path
 * @property string $video_url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class ConsultationTitle extends Eloquent
{
	protected $casts = [
		'has_file' => 'int'
	];

	protected $fillable = [
		'title',
		'description',
		'cover_image',
		'has_file',
		'file_path',
        'video_url'
	];
}
