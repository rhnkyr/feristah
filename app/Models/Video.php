<?php

    /**
     * Created by Reliese Model.
     * Date: Sun, 16 Jun 2019 05:15:43 +0000.
     */

    namespace App\Models;

    use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
    use Reliese\Database\Eloquent\Model as Eloquent;
    use Spatie\Searchable\Searchable;
    use Spatie\Searchable\SearchResult;

    /**
     * Class Video
     *
     * @property int            $id
     * @property int            $video_category_id
     * @property int            $consult_id
     * @property string         $title
     * @property string         $source
     * @property string         $content_path
     * @property string         $description
     * @property int            $has_consult
     * @property int            $status
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Video extends Eloquent implements Searchable
    {

        use Favoriteable;

        protected $casts
            = [
                'video_category_id' => 'int',
                'consult_id'        => 'int',
                'has_consult'       => 'int',
                'status'            => 'int',
            ];

        protected $fillable
            = [
                'video_category_id',
                'consult_id',
                'title',
                'source',
                'content_path',
                'description',
                'has_consult',
                'status',
            ];

        public function category()
        {
            return $this->belongsTo(VideoCategory::class, 'video_category_id');
        }

        public function scopeByCategory($query, $category_id)
        {
            return $query->where('video_category_id', $category_id);
        }

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }

        public function getSearchResult(): SearchResult
        {
            return new SearchResult(
                $this,
                $this->title,
                $this->id
            );
        }
    }
