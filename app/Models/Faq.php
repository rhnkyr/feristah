<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:28:12 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class Faq
     *
     * @property int            $id
     * @property string         $title
     * @property string         $content
     * @property int            $order
     * @property int            $status
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class Faq extends Eloquent
    {
        protected $casts
            = [
                'order'  => 'int',
                'status' => 'int',
            ];

        protected $fillable
            = [
                'title',
                'content',
                'order',
                'status',
            ];

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }

        public function scopeOrder($query)
        {
            return $query->orderBy('order');
        }
    }
