<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 17 May 2019 14:49:46 +0000.
     */

    namespace App\Models;

    use ChristianKuri\LaravelFavorite\Traits\Favoriteable;
    use Reliese\Database\Eloquent\Model as Eloquent;
    use Spatie\Searchable\Searchable;
    use Spatie\Searchable\SearchResult;

    /**
     * Class SocialLifePlace
     *
     * @property int            $id
     * @property int            $social_life_category_id
     * @property string         $name
     * @property string         $content_path
     * @property string         $sub_title
     * @property string         $address
     * @property string         $location
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class SocialLifePlace extends Eloquent implements Searchable
    {

        use Favoriteable;

        protected $casts
            = [
                'social_life_category_id' => 'int',
            ];

        protected $fillable
            = [
                'social_life_category_id',
                'name',
                'content_path',
                'sub_title',
                'address',
                'status',
                'location',
            ];


        public function category()
        {
            return $this->belongsTo(SocialLifeCategory::class, 'social_life_category_id');
        }

        public function scopeByCategory($query, $category_id)
        {
            return $query->where('social_life_category_id', $category_id);
        }

        public function scopeActive($query)
        {
            return $query->where('status', 1);
        }

        public function getSearchResult(): SearchResult
        {
            return new SearchResult(
                $this,
                $this->name,
                $this->id
            );
        }
    }
