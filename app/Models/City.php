<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 17 May 2019 14:28:12 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $id
 * @property string $plate
 * @property string $name_upper
 * @property string $name
 * @property string $name_lower
 *
 * @package App\Models
 */
class City extends Eloquent
{
	public $timestamps = false;

	protected $fillable = [
		'plate',
		'name_upper',
		'name',
		'name_lower'
	];
}
