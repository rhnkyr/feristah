<?php

    /**
     * Created by Reliese Model.
     * Date: Fri, 23 Aug 2019 14:31:19 +0000.
     */

    namespace App\Models;

    use Reliese\Database\Eloquent\Model as Eloquent;

    /**
     * Class HomeScreenElement
     *
     * @property int            $id
     * @property int            $row
     * @property int            $order
     * @property string         $image_path
     * @property string         $content_type
     * @property int            $content_id
     * @property string         $data
     * @property string         $title
     * @property \Carbon\Carbon $created_at
     * @property \Carbon\Carbon $updated_at
     *
     * @package App\Models
     */
    class HomeScreenElement extends Eloquent
    {
        protected $casts
            = [
                'row'        => 'int',
                'order'      => 'int',
                'content_id' => 'int',
            ];

        protected $fillable
            = [
                'row',
                'order',
                'image_path',
                'content_type',
                'content_id',
                'data',
                'title',
            ];

        public function content()
        {
            return $this->morphTo();
        }
    }
