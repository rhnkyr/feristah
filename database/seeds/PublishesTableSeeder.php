<?php

    use Illuminate\Database\Seeder;

    class PublishesTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('publishes')->truncate();

            factory(App\Models\Publish::class, 50)->create();
        }
    }
