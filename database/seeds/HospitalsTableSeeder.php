<?php

    use Illuminate\Database\Seeder;

    class HospitalsTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('hospitals')->truncate();

            factory(App\Models\Hospital::class, 10)->create();
        }
    }
