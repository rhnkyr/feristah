<?php

    use Illuminate\Database\Seeder;

    class FaqsTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('faqs')->truncate();

            factory(App\Models\Faq::class, 10)->create();
        }
    }
