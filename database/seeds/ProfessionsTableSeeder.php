<?php

    use Illuminate\Database\Seeder;

    class ProfessionsTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('professions')->truncate();

            factory(App\Models\Profession::class, 3)->create();
        }
    }
