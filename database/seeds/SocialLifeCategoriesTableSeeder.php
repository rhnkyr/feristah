<?php

    use Illuminate\Database\Seeder;

    class SocialLifeCategoriesTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('social_life_categories')->truncate();

            factory(App\Models\SocialLifeCategory::class, 3)->create();
        }
    }
