<?php

    use Illuminate\Database\Seeder;

    class EventsTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('events')->truncate();

            factory(App\Models\Event::class, 10)->create();
        }
    }
