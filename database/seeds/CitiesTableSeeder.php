<?php

    use Illuminate\Database\Seeder;

    class CitiesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('cities')->truncate();
        
        \DB::table('cities')->insert(array (
            0 => 
            array (
                'id' => 1,
                'plate' => '01',
                'name_upper' => 'ADANA',
                'name' => 'Adana',
                'name_lower' => 'adana',
            ),
            1 => 
            array (
                'id' => 2,
                'plate' => '02',
                'name_upper' => 'ADIYAMAN',
                'name' => 'Adıyaman',
                'name_lower' => 'adıyaman',
            ),
            2 => 
            array (
                'id' => 3,
                'plate' => '03',
                'name_upper' => 'AFYONKARAHİSAR',
                'name' => 'Afyonkarahisar',
                'name_lower' => 'afyonkarahisar',
            ),
            3 => 
            array (
                'id' => 4,
                'plate' => '04',
                'name_upper' => 'AĞRI',
                'name' => 'Ağrı',
                'name_lower' => 'ağrı',
            ),
            4 => 
            array (
                'id' => 5,
                'plate' => '05',
                'name_upper' => 'AMASYA',
                'name' => 'Amasya',
                'name_lower' => 'amasya',
            ),
            5 => 
            array (
                'id' => 6,
                'plate' => '06',
                'name_upper' => 'ANKARA',
                'name' => 'Ankara',
                'name_lower' => 'ankara',
            ),
            6 => 
            array (
                'id' => 7,
                'plate' => '07',
                'name_upper' => 'ANTALYA',
                'name' => 'Antalya',
                'name_lower' => 'antalya',
            ),
            7 => 
            array (
                'id' => 8,
                'plate' => '08',
                'name_upper' => 'ARTVİN',
                'name' => 'Artvin',
                'name_lower' => 'artvin',
            ),
            8 => 
            array (
                'id' => 9,
                'plate' => '09',
                'name_upper' => 'AYDIN',
                'name' => 'Aydın',
                'name_lower' => 'aydın',
            ),
            9 => 
            array (
                'id' => 10,
                'plate' => '10',
                'name_upper' => 'BALIKESİR',
                'name' => 'Balıkesir',
                'name_lower' => 'balıkesir',
            ),
            10 => 
            array (
                'id' => 11,
                'plate' => '11',
                'name_upper' => 'BİLECİK',
                'name' => 'Bilecik',
                'name_lower' => 'bilecik',
            ),
            11 => 
            array (
                'id' => 12,
                'plate' => '12',
                'name_upper' => 'BİNGÖL',
                'name' => 'Bingöl',
                'name_lower' => 'bingöl',
            ),
            12 => 
            array (
                'id' => 13,
                'plate' => '13',
                'name_upper' => 'BİTLİS',
                'name' => 'Bitlis',
                'name_lower' => 'bitlis',
            ),
            13 => 
            array (
                'id' => 14,
                'plate' => '14',
                'name_upper' => 'BOLU',
                'name' => 'Bolu',
                'name_lower' => 'bolu',
            ),
            14 => 
            array (
                'id' => 15,
                'plate' => '15',
                'name_upper' => 'BURDUR',
                'name' => 'Burdur',
                'name_lower' => 'burdur',
            ),
            15 => 
            array (
                'id' => 16,
                'plate' => '16',
                'name_upper' => 'BURSA',
                'name' => 'Bursa',
                'name_lower' => 'bursa',
            ),
            16 => 
            array (
                'id' => 17,
                'plate' => '17',
                'name_upper' => 'ÇANAKKALE',
                'name' => 'Çanakkale',
                'name_lower' => 'çanakkale',
            ),
            17 => 
            array (
                'id' => 18,
                'plate' => '18',
                'name_upper' => 'ÇANKIRI',
                'name' => 'Çankırı',
                'name_lower' => 'çankırı',
            ),
            18 => 
            array (
                'id' => 19,
                'plate' => '19',
                'name_upper' => 'ÇORUM',
                'name' => 'Çorum',
                'name_lower' => 'çorum',
            ),
            19 => 
            array (
                'id' => 20,
                'plate' => '20',
                'name_upper' => 'DENİZLİ',
                'name' => 'Denizli',
                'name_lower' => 'denizli',
            ),
            20 => 
            array (
                'id' => 21,
                'plate' => '21',
                'name_upper' => 'DİYARBAKIR',
                'name' => 'Diyarbakır',
                'name_lower' => 'diyarbakır',
            ),
            21 => 
            array (
                'id' => 22,
                'plate' => '22',
                'name_upper' => 'EDİRNE',
                'name' => 'Edirne',
                'name_lower' => 'edirne',
            ),
            22 => 
            array (
                'id' => 23,
                'plate' => '23',
                'name_upper' => 'ELAZIĞ',
                'name' => 'Elazığ',
                'name_lower' => 'elazığ',
            ),
            23 => 
            array (
                'id' => 24,
                'plate' => '24',
                'name_upper' => 'ERZİNCAN',
                'name' => 'Erzincan',
                'name_lower' => 'erzincan',
            ),
            24 => 
            array (
                'id' => 25,
                'plate' => '25',
                'name_upper' => 'ERZURUM',
                'name' => 'Erzurum',
                'name_lower' => 'erzurum',
            ),
            25 => 
            array (
                'id' => 26,
                'plate' => '26',
                'name_upper' => 'ESKİŞEHİR',
                'name' => 'Eskişehir',
                'name_lower' => 'eskişehir',
            ),
            26 => 
            array (
                'id' => 27,
                'plate' => '27',
                'name_upper' => 'GAZİANTEP',
                'name' => 'Gaziantep',
                'name_lower' => 'gaziantep',
            ),
            27 => 
            array (
                'id' => 28,
                'plate' => '28',
                'name_upper' => 'GİRESUN',
                'name' => 'Giresun',
                'name_lower' => 'giresun',
            ),
            28 => 
            array (
                'id' => 29,
                'plate' => '29',
                'name_upper' => 'GÜMÜŞHANE',
                'name' => 'Gümüşhane',
                'name_lower' => 'gümüşhane',
            ),
            29 => 
            array (
                'id' => 30,
                'plate' => '30',
                'name_upper' => 'HAKKARİ',
                'name' => 'Hakkari',
                'name_lower' => 'hakkari',
            ),
            30 => 
            array (
                'id' => 31,
                'plate' => '31',
                'name_upper' => 'HATAY',
                'name' => 'Hatay',
                'name_lower' => 'hatay',
            ),
            31 => 
            array (
                'id' => 32,
                'plate' => '32',
                'name_upper' => 'ISPARTA',
                'name' => 'Isparta',
                'name_lower' => 'ısparta',
            ),
            32 => 
            array (
                'id' => 33,
                'plate' => '33',
                'name_upper' => 'MERSİN',
                'name' => 'Mersin',
                'name_lower' => 'mersin',
            ),
            33 => 
            array (
                'id' => 34,
                'plate' => '34',
                'name_upper' => 'İSTANBUL',
                'name' => 'İstanbul',
                'name_lower' => 'istanbul',
            ),
            34 => 
            array (
                'id' => 35,
                'plate' => '35',
                'name_upper' => 'İZMİR',
                'name' => 'İzmir',
                'name_lower' => 'izmir',
            ),
            35 => 
            array (
                'id' => 36,
                'plate' => '36',
                'name_upper' => 'KARS',
                'name' => 'Kars',
                'name_lower' => 'kars',
            ),
            36 => 
            array (
                'id' => 37,
                'plate' => '37',
                'name_upper' => 'KASTAMONU',
                'name' => 'Kastamonu',
                'name_lower' => 'kastamonu',
            ),
            37 => 
            array (
                'id' => 38,
                'plate' => '38',
                'name_upper' => 'KAYSERİ',
                'name' => 'Kayseri',
                'name_lower' => 'kayseri',
            ),
            38 => 
            array (
                'id' => 39,
                'plate' => '39',
                'name_upper' => 'KIRKLARELİ',
                'name' => 'Kırklareli',
                'name_lower' => 'kırklareli',
            ),
            39 => 
            array (
                'id' => 40,
                'plate' => '40',
                'name_upper' => 'KIRŞEHİR',
                'name' => 'Kırşehir',
                'name_lower' => 'kırşehir',
            ),
            40 => 
            array (
                'id' => 41,
                'plate' => '41',
                'name_upper' => 'KOCAELİ',
                'name' => 'Kocaeli',
                'name_lower' => 'kocaeli',
            ),
            41 => 
            array (
                'id' => 42,
                'plate' => '42',
                'name_upper' => 'KONYA',
                'name' => 'Konya',
                'name_lower' => 'konya',
            ),
            42 => 
            array (
                'id' => 43,
                'plate' => '43',
                'name_upper' => 'KÜTAHYA',
                'name' => 'Kütahya',
                'name_lower' => 'kütahya',
            ),
            43 => 
            array (
                'id' => 44,
                'plate' => '44',
                'name_upper' => 'MALATYA',
                'name' => 'Malatya',
                'name_lower' => 'malatya',
            ),
            44 => 
            array (
                'id' => 45,
                'plate' => '45',
                'name_upper' => 'MANİSA',
                'name' => 'Manisa',
                'name_lower' => 'manisa',
            ),
            45 => 
            array (
                'id' => 46,
                'plate' => '46',
                'name_upper' => 'KAHRAMANMARAŞ',
                'name' => 'Kahramanmaraş',
                'name_lower' => 'kahramanmaraş',
            ),
            46 => 
            array (
                'id' => 47,
                'plate' => '47',
                'name_upper' => 'MARDİN',
                'name' => 'Mardin',
                'name_lower' => 'mardin',
            ),
            47 => 
            array (
                'id' => 48,
                'plate' => '48',
                'name_upper' => 'MUĞLA',
                'name' => 'Muğla',
                'name_lower' => 'muğla',
            ),
            48 => 
            array (
                'id' => 49,
                'plate' => '49',
                'name_upper' => 'MUŞ',
                'name' => 'Muş',
                'name_lower' => 'muş',
            ),
            49 => 
            array (
                'id' => 50,
                'plate' => '50',
                'name_upper' => 'NEVŞEHİR',
                'name' => 'Nevşehir',
                'name_lower' => 'nevşehir',
            ),
            50 => 
            array (
                'id' => 51,
                'plate' => '51',
                'name_upper' => 'NİĞDE',
                'name' => 'Niğde',
                'name_lower' => 'niğde',
            ),
            51 => 
            array (
                'id' => 52,
                'plate' => '52',
                'name_upper' => 'ORDU',
                'name' => 'Ordu',
                'name_lower' => 'ordu',
            ),
            52 => 
            array (
                'id' => 53,
                'plate' => '53',
                'name_upper' => 'RİZE',
                'name' => 'Rize',
                'name_lower' => 'rize',
            ),
            53 => 
            array (
                'id' => 54,
                'plate' => '54',
                'name_upper' => 'SAKARYA',
                'name' => 'Sakarya',
                'name_lower' => 'sakarya',
            ),
            54 => 
            array (
                'id' => 55,
                'plate' => '55',
                'name_upper' => 'SAMSUN',
                'name' => 'Samsun',
                'name_lower' => 'samsun',
            ),
            55 => 
            array (
                'id' => 56,
                'plate' => '56',
                'name_upper' => 'SİİRT',
                'name' => 'Siirt',
                'name_lower' => 'siirt',
            ),
            56 => 
            array (
                'id' => 57,
                'plate' => '57',
                'name_upper' => 'SİNOP',
                'name' => 'Sinop',
                'name_lower' => 'sinop',
            ),
            57 => 
            array (
                'id' => 58,
                'plate' => '58',
                'name_upper' => 'SİVAS',
                'name' => 'Sivas',
                'name_lower' => 'sivas',
            ),
            58 => 
            array (
                'id' => 59,
                'plate' => '59',
                'name_upper' => 'TEKİRDAĞ',
                'name' => 'Tekirdağ',
                'name_lower' => 'tekirdağ',
            ),
            59 => 
            array (
                'id' => 60,
                'plate' => '60',
                'name_upper' => 'TOKAT',
                'name' => 'Tokat',
                'name_lower' => 'tokat',
            ),
            60 => 
            array (
                'id' => 61,
                'plate' => '61',
                'name_upper' => 'TRABZON',
                'name' => 'Trabzon',
                'name_lower' => 'trabzon',
            ),
            61 => 
            array (
                'id' => 62,
                'plate' => '62',
                'name_upper' => 'TUNCELİ',
                'name' => 'Tunceli',
                'name_lower' => 'tunceli',
            ),
            62 => 
            array (
                'id' => 63,
                'plate' => '63',
                'name_upper' => 'ŞANLIURFA',
                'name' => 'Şanlıurfa',
                'name_lower' => 'şanlıurfa',
            ),
            63 => 
            array (
                'id' => 64,
                'plate' => '64',
                'name_upper' => 'UŞAK',
                'name' => 'Uşak',
                'name_lower' => 'uşak',
            ),
            64 => 
            array (
                'id' => 65,
                'plate' => '65',
                'name_upper' => 'VAN',
                'name' => 'Van',
                'name_lower' => 'van',
            ),
            65 => 
            array (
                'id' => 66,
                'plate' => '66',
                'name_upper' => 'YOZGAT',
                'name' => 'Yozgat',
                'name_lower' => 'yozgat',
            ),
            66 => 
            array (
                'id' => 67,
                'plate' => '67',
                'name_upper' => 'ZONGULDAK',
                'name' => 'Zonguldak',
                'name_lower' => 'zonguldak',
            ),
            67 => 
            array (
                'id' => 68,
                'plate' => '68',
                'name_upper' => 'AKSARAY',
                'name' => 'Aksaray',
                'name_lower' => 'aksaray',
            ),
            68 => 
            array (
                'id' => 69,
                'plate' => '69',
                'name_upper' => 'BAYBURT',
                'name' => 'Bayburt',
                'name_lower' => 'bayburt',
            ),
            69 => 
            array (
                'id' => 70,
                'plate' => '70',
                'name_upper' => 'KARAMAN',
                'name' => 'Karaman',
                'name_lower' => 'karaman',
            ),
            70 => 
            array (
                'id' => 71,
                'plate' => '71',
                'name_upper' => 'KIRIKKALE',
                'name' => 'Kırıkkale',
                'name_lower' => 'kırıkkale',
            ),
            71 => 
            array (
                'id' => 72,
                'plate' => '72',
                'name_upper' => 'BATMAN',
                'name' => 'Batman',
                'name_lower' => 'batman',
            ),
            72 => 
            array (
                'id' => 73,
                'plate' => '73',
                'name_upper' => 'ŞIRNAK',
                'name' => 'Şırnak',
                'name_lower' => 'şırnak',
            ),
            73 => 
            array (
                'id' => 74,
                'plate' => '74',
                'name_upper' => 'BARTIN',
                'name' => 'Bartın',
                'name_lower' => 'bartın',
            ),
            74 => 
            array (
                'id' => 75,
                'plate' => '75',
                'name_upper' => 'ARDAHAN',
                'name' => 'Ardahan',
                'name_lower' => 'ardahan',
            ),
            75 => 
            array (
                'id' => 76,
                'plate' => '76',
                'name_upper' => 'IĞDIR',
                'name' => 'Iğdır',
                'name_lower' => 'ığdır',
            ),
            76 => 
            array (
                'id' => 77,
                'plate' => '77',
                'name_upper' => 'YALOVA',
                'name' => 'Yalova',
                'name_lower' => 'yalova',
            ),
            77 => 
            array (
                'id' => 78,
                'plate' => '78',
                'name_upper' => 'KARABÜK',
                'name' => 'Karabük',
                'name_lower' => 'karabük',
            ),
            78 => 
            array (
                'id' => 79,
                'plate' => '79',
                'name_upper' => 'KİLİS',
                'name' => 'Kilis',
                'name_lower' => 'kilis',
            ),
            79 => 
            array (
                'id' => 80,
                'plate' => '80',
                'name_upper' => 'OSMANİYE',
                'name' => 'Osmaniye',
                'name_lower' => 'osmaniye',
            ),
            80 => 
            array (
                'id' => 81,
                'plate' => '81',
                'name_upper' => 'DÜZCE',
                'name' => 'Düzce',
                'name_lower' => 'düzce',
            ),
        ));
        
        
    }
}
