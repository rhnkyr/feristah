<?php

    use Illuminate\Database\Seeder;

    class VideoCategoryTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('video_categories')->truncate();

            factory(App\Models\VideoCategory::class, 3)->create();
        }
    }
