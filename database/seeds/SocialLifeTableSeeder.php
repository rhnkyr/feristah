<?php

    use Illuminate\Database\Seeder;

    class SocialLifeTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('social_life_places')->truncate();

            factory(App\Models\SocialLifePlace::class, 50)->create();
        }
    }
