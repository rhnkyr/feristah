<?php

    use Illuminate\Database\Seeder;

    class DatabaseSeeder extends Seeder
    {
        /**
         * Seed the application's database.
         *
         * @return void
         */
        public function run()
        {
            // $this->call(UsersTableSeeder::class);
            $this->call(CitiesTableSeeder::class);
            $this->call(HospitalsTableSeeder::class);
            $this->call(ProfessionsTableSeeder::class);
            $this->call(DoctorsTableSeeder::class);
            $this->call(PublishesTableSeeder::class);
            $this->call(SocialLifeCategoriesTableSeeder::class);
            $this->call(SocialLifeTableSeeder::class);
            $this->call(VideoCategoryTableSeeder::class);
            $this->call(VideoTableSeeder::class);
            $this->call(EventsTableSeeder::class);
            $this->call(FaqsTableSeeder::class);
        }
    }
