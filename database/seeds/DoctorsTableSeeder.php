<?php

    use Illuminate\Database\Seeder;

    class DoctorsTableSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            \DB::table('doctors')->truncate();

            factory(App\Models\Doctor::class, 50)->create();
        }
    }
