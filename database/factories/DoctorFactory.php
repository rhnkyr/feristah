<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\Doctor;
    use Faker\Generator as Faker;

    $factory->define(Doctor::class, function (Faker $faker) {

        $name = $faker->name;

        $professions = [1, 2, 3];
        shuffle($professions);

        $hospitals = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
        shuffle($hospitals);

        return [
            'profession_id' => $professions[0],
            'hospital_id'   => $hospitals[0],
            'city_id'       => 34,
            'name'          => $name,
            'content_path'  => 'doctors/' . \Str::slug($name),
            'sub_title'     => $faker->title,
            'status'        => 1,
        ];
    });
