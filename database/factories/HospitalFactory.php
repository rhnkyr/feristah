<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\Hospital;
    use Faker\Generator as Faker;

    $factory->define(Hospital::class, function (Faker $faker) {

        $name = $faker->company;

        return [
            'city_id'      => 34,
            'title'        => $name,
            'content_path' => 'hospitals/' . \Str::slug($name),
            'sub_title'    => 'Sub Title',
            'address'      => $faker->address,
            'location'     => $faker->latitude . '-' . $faker->longitude,
            'status'       => 1,
        ];
    });
