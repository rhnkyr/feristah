<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\Faq;
    use Faker\Generator as Faker;

    $factory->define(Faq::class, function (Faker $faker) {
        return [
            'title'   => $faker->sentence(3),
            'content' => $faker->sentence(25),
            'order'   => 1,
            'status'  => 1,
        ];
    });
