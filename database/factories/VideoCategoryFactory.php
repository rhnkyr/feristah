<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\VideoCategory;
    use Faker\Generator as Faker;

    $factory->define(VideoCategory::class, function (Faker $faker) {
        return [
            'title'  => $faker->sentence(1),
            'status' => 1,
        ];
    });
