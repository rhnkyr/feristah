<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\Video;
    use Faker\Generator as Faker;

    $factory->define(Video::class, function (Faker $faker) {

        $title      = $faker->sentence(2);
        $categories = [1, 2, 3];
        shuffle($categories);

        return [
            'video_category_id' => $categories[0],
            'title'             => $title,
            'consult_id'        => 1,
            'content_path'      => 'videos/' . \Str::slug($title),
            'source'            => 'https://vimeo.com/76979871',
            'description'       => $faker->sentence(15),
            'has_consult'       => 1,
            'status'            => 1,
        ];
    });
