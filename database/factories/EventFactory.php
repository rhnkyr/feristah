<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\Event;
    use Faker\Generator as Faker;

    $factory->define(Event::class, function (Faker $faker) {

        $title = $faker->sentence(2);

        $colors = [
            'FFFF00',
            '000000',
            'FF0000',
            '008000',
        ];

        shuffle($colors);

        $dates = [
            '2019-06-08 17:00:00',
            '2019-06-09 13:00:00',
            '2019-06-10 14:00:00',
            '2019-06-11 09:00:00',
            '2019-06-12 10:00:00',
            '2019-06-13 11:00:00',
            '2019-06-14 16:00:00',
        ];

        shuffle($dates);

        return [
            'title'        => $title,
            'content_path' => 'publishes/' . \Str::slug($title),
            'image_path'   => 'https://via.placeholder.com/150/' . $colors[0] . '/FFFFFF',
            'description'  => $faker->sentence(25),
            'status'       => 1,
            'when'         => $faker->dateTimeBetween('+0 days', '+1 years')->format('d-m-Y H:i')//$dates[0],
        ];
    });
