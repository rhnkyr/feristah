<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\SocialLifeCategory;
    use Faker\Generator as Faker;

    $factory->define(SocialLifeCategory::class, function (Faker $faker) {

        $colors = [
            'FFFF00',
            '000000',
            'FF0000',
            '008000',
        ];

        shuffle($colors);

        return [
            'title'      => $faker->sentence(2),
            'icon_path'  => 'https://img.icons8.com/material-outlined/24/000000/message-link.png',
            'image_path' => 'https://via.placeholder.com/150/' . $colors[0] . '/FFFFFF',
            'status'     => 1,
            'order'      => 1,
        ];
    });
