<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\Publish;
    use Faker\Generator as Faker;

    $factory->define(Publish::class, function (Faker $faker) {

        $title = $faker->sentence(2);

        return [
            'title'        => $title,
            'content_path' => 'publishes/' . \Str::slug($title),
            'description'  => $faker->sentence(25),
            'file_path'    => 'files/' . \Str::slug($title) . '.pdf',
            'status'       => 1,
            'order'        => 1,
        ];
    });
