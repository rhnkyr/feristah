<?php

    /* @var $factory \Illuminate\Database\Eloquent\Factory */

    use App\Models\SocialLifePlace;
    use Faker\Generator as Faker;

    $factory->define(SocialLifePlace::class, function (Faker $faker) {

        $name = $faker->sentence(2);

        $categories = [1, 2, 3];
        shuffle($categories);

        return [
            'social_life_category_id' => $categories[0],
            'name'                    => $name,
            'content_path'            => 'places/' . \Str::slug($name),
            'sub_title'               => 'Sub Title',
            'address'                 => $faker->address,
            'location'                => $faker->latitude . '-' . $faker->longitude,
            'status'                  => 1,
        ];
    });
