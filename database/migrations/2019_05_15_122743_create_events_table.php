<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateEventsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('events', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->string('title');
                $table->string('content_path');
                $table->string('image_path')->nullable();
                $table->text('description');
                $table->tinyInteger('status')->unsigned();
                $table->dateTime('when');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('events');
        }
    }
