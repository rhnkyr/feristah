<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateUsersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('users', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('identifier');
                $table->integer('type')->default(1)->nullable()->comment('1- User, 2-Editor, 3-Admin etc...');
                $table->string('user_name');
                $table->string('one_signal_id')->nullable();
                $table->date('birthday')->nullable();
                $table->string('email')->unique();
                $table->string('city')->nullable();
                $table->string('country')->nullable();
                $table->string('hear_us')->nullable();
                $table->tinyInteger('password_reset')->default(0);
                $table->tinyInteger('is_kvk_accepted')->nullable()->default(0);
                $table->timestamp('email_verified_at')->nullable();
                $table->string('password');
                $table->rememberToken();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('users');
        }
    }
