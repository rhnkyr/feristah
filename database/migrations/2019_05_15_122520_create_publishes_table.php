<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreatePublishesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('publishes', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->string('title');
                $table->string('content_path');
                $table->text('description')->nullable();
                $table->string('file_path');
                $table->tinyInteger('status')->unsigned();
                $table->tinyInteger('order')->unsigned()->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('publishes');
        }
    }
