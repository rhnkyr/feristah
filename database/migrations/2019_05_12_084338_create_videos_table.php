<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateVideosTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('videos', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->tinyInteger('video_category_id')->unsigned();
                $table->string('content_path');
                $table->string('source');
                $table->text('description');
                $table->tinyInteger('has_consult')->nullable()->unsigned();
                $table->tinyInteger('consult_id')->nullable()->unsigned();
                $table->tinyInteger('status')->unsigned();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('videos');
        }
    }
