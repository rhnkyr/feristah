<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateHospitalsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('hospitals', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->tinyInteger('city_id')->unsigned();
                $table->string('title');
                $table->string('content_path');
                $table->string('sub_title');
                $table->text('address');
                $table->string('location');
                $table->tinyInteger('status')->unsigned();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('hospitals');
        }
    }
