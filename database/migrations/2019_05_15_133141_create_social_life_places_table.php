<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateSocialLifePlacesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('social_life_places', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->integer('social_life_category_id')->unsigned();
                $table->string('content_path');
                $table->string('name');
                $table->string('sub_title');
                $table->text('address');
                $table->string('location');
                $table->tinyInteger('status')->unsigned()->default(1);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('social_life_places');
        }
    }
