<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateRemindersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('reminders', function (Blueprint $table) {
                $table->bigIncrements('id')->unsigned();
                $table->integer('user_id')->unsigned();
                $table->string('title');
                $table->text('description');
                $table->tinyInteger('has_image')->unsigned();
                $table->dateTime('remind_at');
                $table->tinyInteger('reminded')->default(0)->unsigned();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('reminders');
        }
    }
