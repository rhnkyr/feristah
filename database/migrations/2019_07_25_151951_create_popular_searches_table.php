<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreatePopularSearchesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('popular_searches', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('keyword')->index();
                $table->string('slug')->index();
                $table->integer('hit');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('popular_searches');
        }
    }
