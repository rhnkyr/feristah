<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateSocialLifeCategoriesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('social_life_categories', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->string('title');
                $table->string('icon_path');
                $table->string('image_path');
                $table->tinyInteger('status')->unsigned();
                $table->tinyInteger('order')->unsigned()->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('social_life_categories');
        }
    }
