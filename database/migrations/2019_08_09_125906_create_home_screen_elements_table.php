<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class CreateHomeScreenElementsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('home_screen_elements', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->unsignedTinyInteger('row');
                $table->unsignedTinyInteger('order');
                $table->morphs('content');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('home_screen_elements');
        }
    }
