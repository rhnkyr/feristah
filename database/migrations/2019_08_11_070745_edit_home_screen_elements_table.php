<?php

    use Illuminate\Database\Migrations\Migration;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Support\Facades\Schema;

    class EditHomeScreenElementsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::table('home_screen_elements', function (Blueprint $table) {
                $table->string('image_path');
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::table('home_screen_elements', function (Blueprint $table) {
                $table->removeColumn('image_path');
            });
        }
    }
